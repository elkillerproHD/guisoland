export const colors = {
  "normal-primary": '#ff66b6',
  "normal-secondary": '#ffb2c7',
  white: '#fff',
  darkgray: '#464646',
  black: '#000'
};
