
export const strings = {
  Home_Layout1_name: "Guisoland",
  Home_Name_In_Card_Header: "Guisoland",
  Home_Data_In_Card_220: {
    bottomButton: "Comprar ya - $220",
    foodText: "1 comida",
    sideMealText: "1 acompañamiento",
    drinkText: "1 bebida",
  },
  Home_Data_In_Card_149: {
    bottomButton: "Comprar ya - $149 c/u",
    foodText: "1 comida",
    sideMealText: "1 acompañamiento",
    drinkText: "1 bebida",
  },
  Home_Data_In_Card_110: {
    bottomButton: "Comprar ya - $110 c/u",
    foodText: "1 comida",
    sideMealText: "1 acompañamiento",
    drinkText: "1 bebida",
  },
  Home_Buy_Now_In_Card: "Reservar ahora",
  Test_1Food_For_Card: "1 Comida",
  Home_Layout1_TopCallToActionText1: "Ahorra dinero pero sobre todo,",
  Home_Layout1_TopCallToActionText2: "gana tiempo con Guisoland.",
  Home_Layout1_10PercentageOff: "10% OFF",
  Home_Layout2_BigText: "Guisoland",
  Home_Layout2_SubTitle1: "Rico, mucho y barato.",
  Home_Layout2_SubTitle2: "Ya sabemos, es perfecto",
  Open_SingBoard: "Abierto",
  Close_SingBoard: "Cerrado",
  FooterData: {
    telephone: {
      text: 'Tel: ',
      data: '+54 911 5055 - 2119'
    },
    email: {
      text: 'Email: ',
      data: 'hola@guisoland.xyz'
    }
  },
  PrivacyDataText: 'Federico Fernández © 2020',
  PrivacyDataTextLink: 'Política de Privacidad',
  FooterFunnyText1: '¿Lo mejor de robarle a la abuela?',
  FooterFunnyText2: 'Ahora tenemos sus recetas',
  Hashtags: [
    "#IgualQueLaComidaDeLaABuela",
    "#CompartinosTusComidas",
    "#CompartinosTuGuiso",
  ],
  WeAreHere: 'Nos encontras aca:',
  MenusFound: 'Platillos encontrados: ',
  LoginModalHeaderText: 'Guisoland',
  loginRegisterStrings: {
    logIn: "Loguearse",
    singUp: "Registrarse",
    or: "Sino, tambien puedes...",
    emailError: "Debes ingresar un email valido",
    passError: "La contraseña debe tener entre 8 a 16 caracteres",
    userError: "Debes de ingresar un nombre de usuario",
  }
};
