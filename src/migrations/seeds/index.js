import "regenerator-runtime/runtime.js";

import {GetSeed as foodCategorySeed} from "./foodCategory";
import {GetSeed as foodMenuSeed} from "./foodMenu";
import {MultipleWriteJson} from "../../models/baseModel";
import Firebase from 'firebase';

export default function CreateSeeds() {
  const json = [
      ...foodCategorySeed(),
      ...foodMenuSeed(),
  ]
  MultipleWriteJson({isFront: false, data: json})
}
let firebaseConfig = {
  // apiKey: "AIzaSyD7gdyNA7XY8EVZ2VQ65Zm5KV8QS7pat8w",
  // authDomain: "guisoland-b369a.firebaseapp.com",
  // databaseURL: "https://guisoland-b369a.firebaseio.com",
  projectId: "guisoland-b369a",
  // storageBucket: "guisoland-b369a.appspot.com",
  // messagingSenderId: "71568274698",
  // appId: "1:71568274698:web:b43a6b70eb8aebc5f976f3",
  // measurementId: "G-R63NW4XLXH"
};
Firebase.initializeApp(firebaseConfig);
//   console.log(imagePath)
// }
CreateSeeds();
