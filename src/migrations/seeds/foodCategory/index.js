import {model as checkModel, collection} from '../../../models/foodCategory'
import Path from 'path';

const imageDirPath = `${__dirname}/images`

const pastaImg = "pasta.jpg"
const sandwitchImg = "sandwitch.jpg"
const guisoImg = "guiso.png"
const carneImg = "carne.jpg"
const milanesaImg = "milanesa.jpg"
const empanadaImg = "empanada.png"
const hamburgerImg = "hamburgesa.jpg"
const faturaImg = "medialuna.jpg"
const ensaladaImg = "ensalada.jpg"
const sushiImg = "sushi.png"
const bebidaImg = "bebida.png"
const pizzaImg = "pizza.png"
const heladoImg = "helado.png"

export function GetSeed(){
  const json = [
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "carnes-category",
        name: "Carnes",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: carneImg,
          path: `${imageDirPath}/${carneImg}`,
          fileType: "image/jpg"
        }
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "milanesas-category",
        name: "Milanesas",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: milanesaImg,
          path: `${imageDirPath}/${milanesaImg}`,
          fileType: "image/jpg"
        }
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "sushi-category",
        name: "Sushi",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiImg,
          path: `${imageDirPath}/${sushiImg}`,
          fileType: "image/png"
        }
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "pastas-category",
        name: "Pastas",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: pastaImg,
          path: `${imageDirPath}/${pastaImg}`,
          fileType: "image/jpg"
        }
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "sandwitchs-category",
        name: "Sandwitchs",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sandwitchImg,
          path: `${imageDirPath}/${sandwitchImg}`,
          fileType: "image/jpg"
        }
      }
    },
    // {
    //   check: checkModel,
    //   model: collection,
    //   action: "set",
    //   data: {
    //     id: "guisos-category",
    //     name: "Guisos",
    //     img: {
    //       isImage: true,
    //       sizes: [
    //         128,264,600,1000
    //       ],
    //       file: true,
    //       name: guisoImg,
    //       path: `${imageDirPath}/${guisoImg}`,
    //       fileType: "image/png"
    //     }
    //   }
    // },
    // {
    //   check: checkModel,
    //   model: collection,
    //   action: "set",
    //   data: {
    //     id: "empanadas-category",
    //     name: "Empanadas",
    //     img: {
    //       isImage: true,
    //       sizes: [
    //         128,264,600,1000
    //       ],
    //       file: true,
    //       name: empanadaImg,
    //       path: `${imageDirPath}/${empanadaImg}`,
    //       fileType: "image/png"
    //     }
    //   }
    // },
    // {
    //   check: checkModel,
    //   model: collection,
    //   action: "set",
    //   data: {
    //     id: "hamburguesas-category",
    //     name: "Hamburguesas",
    //     img: {
    //       isImage: true,
    //       sizes: [
    //         128,264,600,1000
    //       ],
    //       file: true,
    //       name: hamburgerImg,
    //       path: `${imageDirPath}/${hamburgerImg}`,
    //       fileType: "image/jpg"
    //     }
    //   }
    // },
    // {
    //   check: checkModel,
    //   model: collection,
    //   action: "set",
    //   data: {
    //     id: "dulces-category",
    //     name: "Dulces y Faturas",
    //     img: {
    //       isImage: true,
    //       sizes: [
    //         128,264,600,1000
    //       ],
    //       file: true,
    //       name: faturaImg,
    //       path: `${imageDirPath}/${faturaImg}`,
    //       fileType: "image/jpg"
    //     }
    //   }
    // },
    // {
    //   check: checkModel,
    //   model: collection,
    //   action: "set",
    //   data: {
    //     name: "Vegetariana",
    //     img: {
    //       isImage: true,
    //       sizes: [
    //         128,264,600,1000
    //       ],
    //       file: true,
    //       name: "string",
    //       path: "string",
    //       fileType: "string"
    //     }
    //   }
    // },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "ensaladas-category",
        name: "Ensaladas",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: ensaladaImg,
          path: `${imageDirPath}/${ensaladaImg}`,
          fileType: "image/jpg"
        }
      }
    },
  //   {
  //     check: checkModel,
  //     model: collection,
  //     action: "set",
  //     data: {
  //       id: "pizza-category",
  //       name: "Pizza",
  //       img: {
  //         isImage: true,
  //         sizes: [
  //           128,264,600,1000
  //         ],
  //         file: true,
  //         name: pizzaImg,
  //         path: `${imageDirPath}/${pizzaImg}`,
  //         fileType: "image/png"
  //       }
  //     }
  //   },
  //   {
  //     check: checkModel,
  //     model: collection,
  //     action: "set",
  //     data: {
  //       id: "helado-category",
  //       name: "Helado",
  //       img: {
  //         isImage: true,
  //         sizes: [
  //           128,264,600,1000
  //         ],
  //         file: true,
  //         name: heladoImg,
  //         path: `${imageDirPath}/${heladoImg}`,
  //         fileType: "image/png"
  //       }
  //     }
  //   },
  ]
  return json;
}
