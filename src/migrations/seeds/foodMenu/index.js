import {model as checkModel, collection} from '../../../models/foodMenu'

const carnesDirPath = `${__dirname}/images/carnes`
const milanesasDirPath = `${__dirname}/images/milanesas`
const sushiDirPath = `${__dirname}/images/sushi`

const albondigasImg = "albondigas.jpg"
const chinchulinesImg = "chinchulines.jpg"
const choripanImg = "choripan.jpg"
const costillaCerdoImg = "costillaCerdo.jpg"
const hamburguesaImg = "hamburguesa.png"
const mollejaImg = "molleja.jpg"
const polloLimonImg = "polloLimon.jpg"
const polloVinoTintoImg = "polloVinoTinto.jpg"
const vacioAsadoImg = "vacio.jpg"

const milanesaImg = "milanesa.png"
const milanesaNapolitanaImg = "milanesaNapolitana.jpg"

const sushiRolApaltadoImg = "rolApaltado.jpg"
const sushiRolClerckImg = "rolClerck.jpg"
const sushiRolBsAsImg = "rolBsAs.jpg"
const sushiRolNewYorkImg = "rolNewYork.jpg"
const sushiPhiladelphiaImg = "rolPhiladelphia.jpg"

export function GetSeed(){
  const json = [
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-albondigas",
        name: "Albondigas",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: albondigasImg,
          path: `${carnesDirPath}/${albondigasImg}`,
          fileType: "image/jpg"
        },
        descrip: "Cumulos de carne y sabor, tienen forma de pelota, no te ahogues...",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Papas",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-chinchulines",
        name: "Chinchulines",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: chinchulinesImg,
          path: `${carnesDirPath}/${chinchulinesImg}`,
          fileType: "image/jpg"
        },
        descrip: "Solo hay 2 tipos de chinchu, o te salen bien o no. Si son malos no son nuestros.",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Chinchus",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-choripan",
        name: "Choripan",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: choripanImg,
          path: `${carnesDirPath}/${choripanImg}`,
          fileType: "image/jpg"
        },
        descrip: "Conoci un extrangero, la unica palabra que aprendio en su estadia fue Choripan. Vofi",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Choripan",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-costillaCerdo",
        name: "Costilla de cerdo",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: costillaCerdoImg,
          path: `${carnesDirPath}/${costillaCerdoImg}`,
          fileType: "image/jpg"
        },
        descrip: "Mi mami me hace esto con mucho amor, por eso no puedo decir que no me gusta...",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Costilla",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-hamburguesa",
        name: "Hamburguesa",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: hamburguesaImg,
          path: `${carnesDirPath}/${hamburguesaImg}`,
          fileType: "image/png"
        },
        descrip: "No hay mucho que decir, hay una pelicula en donde llueve estas ricuras",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 hamburguesa",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-molleja",
        name: "Molleja",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: mollejaImg,
          path: `${carnesDirPath}/${mollejaImg}`,
          fileType: "image/jpg"
        },
        descrip: "Esto es una de las cosas que no puede faltar en el asado",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Molleja",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-polloLimon",
        name: "Pollo al limon",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: polloLimonImg,
          path: `${carnesDirPath}/${polloLimonImg}`,
          fileType: "image/jpg"
        },
        descrip: "Que finura, que elegancia, tiene hasta un nombre que alega nivel. Con finas hiervas...",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Pollito",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-vinoTinto",
        name: "Pollo al vino tinto",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: polloVinoTintoImg,
          path: `${carnesDirPath}/${polloVinoTintoImg}`,
          fileType: "image/jpg"
        },
        descrip: "Mi favorito desde lejos, se hace facil y es rico, que digo rico. Esta buenisimo...",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Pollito",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-vacioAsado",
        name: "Vacio asado",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: vacioAsadoImg,
          path: `${carnesDirPath}/${vacioAsadoImg}`,
          fileType: "image/jpg"
        },
        descrip: "Vacio es lo contrario que vas a quedar al comer esta exquisites",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Vacio",
        discout: 10,
        tagsId: [],
        categoriesIds: ["carnes-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-milanesa",
        name: "Milanesa",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: milanesaImg,
          path: `${milanesasDirPath}/${milanesaImg}`,
          fileType: "image/png"
        },
        descrip: "Es de todas las comidas la más presente en el menú, es más argentina que el puchero con caracú.",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Mila",
        discout: 10,
        tagsId: [],
        categoriesIds: ["milanesas-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-milanesaNapo",
        name: "Milanesa Napolitana",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: milanesaNapolitanaImg,
          path: `${milanesasDirPath}/${milanesaNapolitanaImg}`,
          fileType: "image/jpg"
        },
        descrip: "Este plato, es Argentino, se dice que nacio en siglo XX en el restaurante de Segundo Napoli",
        price: 330,
        lowerPrice: 140,
        smallWord: "1 Mila",
        discout: 10,
        tagsId: [],
        categoriesIds: ["milanesas-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-sushiApaltado",
        name: "Sushi Rol Apaltado",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiRolApaltadoImg,
          path: `${sushiDirPath}/${sushiRolApaltadoImg}`,
          fileType: "image/jpg"
        },
        descrip: "Roll relleno de langostinos rebozados queso philadelphia, cubierto con palta.",
        price: 330,
        lowerPrice: 140,
        smallWord: "5 rol apaltado",
        discout: 10,
        tagsId: [],
        categoriesIds: ["sushi-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-sushiBsAs",
        name: "Sushi Rol Buenos Aires",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiRolBsAsImg,
          path: `${sushiDirPath}/${sushiRolBsAsImg}`,
          fileType: "image/jpg"
        },
        descrip: "Roll relleno de queso philadelphia y langostinos rebozados cubierto con una fina capa de salmón",
        price: 330,
        lowerPrice: 140,
        smallWord: "5 rol BsAs",
        discout: 10,
        tagsId: [],
        categoriesIds: ["sushi-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-sushiClerck",
        name: "Sushi Rol Clerck",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiRolClerckImg,
          path: `${sushiDirPath}/${sushiRolClerckImg}`,
          fileType: "image/jpg"
        },
        descrip: "Roll relleno de salmón cocido con miel y mostaza, queso phila e hilos de batata frita por fuera",
        price: 330,
        lowerPrice: 140,
        smallWord: "5 rol Clerck",
        discout: 10,
        tagsId: [],
        categoriesIds: ["sushi-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-sushiNY",
        name: "Sushi Rol New York",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiRolNewYorkImg,
          path: `${sushiDirPath}/${sushiRolNewYorkImg}`,
          fileType: "image/jpg"
        },
        descrip: "Roll relleno de salmón y palta, cubierto con sesamo tostado.",
        price: 330,
        lowerPrice: 140,
        smallWord: "5 rol NY",
        discout: 10,
        tagsId: [],
        categoriesIds: ["sushi-category"]
      }
    },
    {
      check: checkModel,
      model: collection,
      action: "set",
      data: {
        id: "food-menu-sushiPhiladelphia",
        name: "Sushi Rol Philadelphia",
        img: {
          isImage: true,
          sizes: [
            128,264,600,1000
          ],
          file: true,
          name: sushiPhiladelphiaImg,
          path: `${sushiDirPath}/${sushiPhiladelphiaImg}`,
          fileType: "image/jpg"
        },
        descrip: "Roll relleno de salmón y queso philadelphia, cubierto con sésamo tostado.",
        price: 330,
        lowerPrice: 140,
        smallWord: "5 rol NY",
        discout: 10,
        tagsId: [],
        categoriesIds: ["sushi-category"]
      }
    },
  ]
  return json;
}
