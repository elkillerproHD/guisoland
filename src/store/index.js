import HomeReducer,{HomeReducerName} from "../actions/home";
import {applyMiddleware, createStore, combineReducers} from 'redux';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  [HomeReducerName]: HomeReducer,
});

const store =
  createStore(
    rootReducer,
    applyMiddleware(thunk)
  );

export default store;
