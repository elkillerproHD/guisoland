import React from 'react';
import {Background} from "../components/containers/background";
import {Input} from "../components/input";
import styled from "styled-components";
import SearchIcon from "../icons/js/search.svg";
import {colors} from "../constants/colors";

export const InputStorie = () => {
  const Container = styled.div `
    width: 60%;
    padding-left: 20%;
    padding-right: 20%;
    display: flex;
    justify-content: center;
  `
  return (
    <Background color={"#fff"}>
      <Container>
        <Input
          // value={state[input.name]}
          // onChange={(e) => this.SetInputData(e, input)}
          text={"Input"}
          leftIcon={
            <SearchIcon width={22} height={22} fill={colors["normal-secondary"]} />
          }
        />
      </Container>
    </Background>
  )
};

export default {
  title: 'Inputs',
};
