import React, {useEffect, useState} from 'react';
import {colors} from "../constants/colors";
import {SliderComponent} from "../components/slider";
import CategoryVerticalDraggableSlider from "../components/slider/categoryVerticalDraggableSlider";
import InitFirebase from "../functions/initFirebase";
import {FoodMenu} from "../models/foodMenu";
import {devStorageServerUrl, storageServer} from "../constants/urls";
import {FoodCategory} from "../models/foodCategory";
import BuyPageMenuFoodSlider from "../components/slider/buyPageSlider";


export const Slider = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh" }}>
    <SliderComponent />
  </div>
);

export const SliderCategory = () => {
  const [query, setQuery] = useState([]);
  const [selected, setSelected] = useState(0);
  let url;
  const nodeEnv = process.env.NODE_ENV || "production";
  if(nodeEnv === "development"){
    url = `${devStorageServerUrl}/264/`;
  } else {
    url = `${storageServer}/264/`;
  }
  const Init = async () => {
    await InitFirebase();
    const foodCategory = new FoodCategory({})
    await foodCategory.all();
    setQuery(foodCategory.values);
  }
  useEffect(() => {
    Init().then();
  }, []);
  const click = (cat, index) => {
    setSelected(index)
  }
  return(
    <div style={{marginLeft: "1%", marginTop: "10vh"}}>
      <CategoryVerticalDraggableSlider
        selected={selected}
        onClick={click}
        url={url}
        categories={query}
      />
    </div>
  )
};

export const BuyPageFoodMenuSliderStorie = () => {
  return(
    <BuyPageMenuFoodSlider />
  )
}

export default {
  title: 'Slider',
};

