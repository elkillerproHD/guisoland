import React from 'react';
import HeaderComponent from "../components/header";
import {MenusFiltersComponent} from "../components/menusFilters";
import {colors} from "../constants/colors";

export const Header = () => (
  <HeaderComponent />
);
export const MenusFilters = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh" }}>
    <MenusFiltersComponent />
  </div>
);

export default {
  title: 'Header',
};

