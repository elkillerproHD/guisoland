import React from 'react';
import LoginRegisterModal from "../components/modal/loginRegister";
import {Background} from "../components/containers/background";
import {colors} from "../constants/colors";

export const LoginRegisterModalStory = () => [
  <LoginRegisterModal />,
  <Background color={colors["normal-primary"]} />
];

export default {
  title: 'Modals',
};
