import React from 'react';
import {colors} from "../constants/colors";
import {MapForLanding} from "../components/maps";

export const MapsForLanding = () => (
  <MapForLanding />
);

export default {
  title: 'Maps',
};

