import React from 'react';
import { Background } from '../components/containers/background'
import {colors} from "../constants/colors";
import {BackgroundImage} from "../components/containers/backgroundImage";

export const BackgroundContainer = () => (
  <Background hasTestBorder color={colors["normal-primary"]} />
);

export const BackgroundImageContainer = () => (
  <BackgroundImage />
);

export default {
  title: 'Containers',
  component: Text,
};

