import React from 'react';
import {Background} from "../components/containers/background";
import styled from "styled-components";
import {CuteKitty} from "../components/animations/cuteKitty";
import {KittyArmWithFlag} from "../components/animations/kittyArm";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  position: relative;
`

export const CuteKittenAnimation = () => [
  <Background color={"#fff"} >
    <Container>
      <CuteKitty />
      <KittyArmWithFlag />
    </Container>
  </Background>
];

export default {
  title: 'Animations',
};
