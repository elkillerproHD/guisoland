import React, { useState } from 'react';
import {colors} from "../constants/colors";
import {OrdersCompletedComponent} from "../components/buttons";
import {OpenCloseButtonIcon} from "../icons/js/openCloseButton";
import {MultiSelectorsButton} from "../components/buttons/multiSelector";
import {Card} from "../components/modal/loginRegister/styles";
import {CircleButton} from "../components/buttons/circleButton";
import FilterIcon from "../icons/js/filter.svg";


export const OrdersCompleted = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh" }}>
    <OrdersCompletedComponent />
  </div>
);

export const FloatOpenCloseButton = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh" }}>
    <OpenCloseButtonIcon />
  </div>
);

export const CircleButtonStorie = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh" }}>
    <CircleButton >
      <FilterIcon width={28} height={28} fill={"#fff"} />
    </CircleButton>
  </div>
);

export const MultiSelectorButton = () => {
  let [stateSelector, setStateSelector] = useState(0)
  const Buttons = [
    "Option 1",
    "Option 2"
  ];
  const SetLoginOrSingUpAction = () => {
    if(stateSelector === 0){
      setStateSelector(1)
    } else {
      setStateSelector(0)
    }
  };
  return(
    <MultiSelectorsButton onChange={SetLoginOrSingUpAction} selected={stateSelector} buttons={Buttons} />
  )
}

export default {
  title: 'Buttons',
};

