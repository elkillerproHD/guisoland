import React from 'react';
import { CardHomeLanding } from '../components/cards/cardHomeLanding'
import styled from "styled-components";
import {FoodCardComponent} from "../components/cards/foodCard";
import {ModalCornerAlertComponent} from "../components/cards/modalCorner";
import {BigCardHomeLanding} from "../components/cards/cardHomeLanding/big";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const MenuCardStorie = () => (
  <Container>
    <CardHomeLanding />
  </Container>
);

export const BigMenuCardStorie = () => (
  <Container>
    <BigCardHomeLanding />
  </Container>
);

export const ModalCorner = () => (
  <Container>
    <ModalCornerAlertComponent />
  </Container>
);

export const FoodCard = () => (
  <Container>
    <FoodCardComponent />
  </Container>
);

export default {
  title: 'Cards',
};

