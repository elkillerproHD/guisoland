import React from 'react';
import HomeLayout1 from "../pages/home/layout-1";
import {HomeLayout2} from "../pages/home/layout-2";
import {HomeLayout3} from "../pages/home/layout-3";
import {KittyLayout} from "../pages/home/kittyLayout";

export const HomePageLayout1 = () => (
  <HomeLayout1 />
);
export const HomePageLayout2 = () => (
  <HomeLayout2 />
);

export const HomePageLayout3 = () => (
  <HomeLayout3 />
);

export const KittyLayoutSt = () => (
  <KittyLayout />
);

export default {
  title: 'Pages',
};

