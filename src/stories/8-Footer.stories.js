import React from 'react';
import {colors} from "../constants/colors";
import {FooterComponent} from "../components/footer";

export const Footer = () => (
  <div style={{ width: 100 + "vw", height: 95 + "vh", backgroundColor: colors["normal-secondary"], paddingTop: 5 + "vh", display: "flex", justifyContent: "center", alignItems: 'center' }}>
    <FooterComponent />
  </div>
);

export default {
  title: 'Footer',
};

