import React from 'react';
import Home from "./pages/home";
import './animations.css'
import {Provider} from "react-redux";
import Store from './store'
import {UploadFileToFirebaseFront} from "./functions/uploadFileToFirebase";
import Firebase from 'firebase';
import BuyPage from "./pages/buyPage";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  // Initialize Cloud Firestore through Firebase
  let firebaseConfig = {
    // apiKey: "AIzaSyD7gdyNA7XY8EVZ2VQ65Zm5KV8QS7pat8w",
    // authDomain: "guisoland-b369a.firebaseapp.com",
    // databaseURL: "https://guisoland-b369a.firebaseio.com",
    projectId: "guisoland-b369a",
    // storageBucket: "guisoland-b369a.appspot.com",
    // messagingSenderId: "71568274698",
    // appId: "1:71568274698:web:b43a6b70eb8aebc5f976f3",
    // measurementId: "G-R63NW4XLXH"
  };
  Firebase.initializeApp(firebaseConfig);
  return (
    <Provider store={Store} >
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/buy">
            <BuyPage />
          </Route>
        </Switch>
      </Router>
      {/*<input type={"file"} onChange={(e) => {UploadFileToFirebaseFront({file: e.target.files[0]})}} />*/}
    </Provider>
  );
}

export default App;
