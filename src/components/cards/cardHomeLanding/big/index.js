import React, { useState } from 'react';
import {colors} from "../../../../constants/colors";
import {
  BottomLayout,
  Container,
  MiddleLayout,
  TopLayout,
  Logo,
  ContainerLogoTextAndSwitchInHeader,
  NameForLogoInHeader,
  ContainerFoodImagesInHeader,
  FoodImageInHeader,
  ButtonTextInBottom,
  BuyNowButtonInMiddleContainer,
  BuyNowButtonInMiddleText,
  IncognitaSymbolInMiddle,
  IncognitaSymbolContainer,
  MenuWithImageTickAndTextContainer,
  ImageAndTextOfMenu,
  TickOfMenu,
  ImagesInMenu,
  TextInMenu,
  ContainerLogoText,
  ContainerSwitchInHeader,
  TextSwitchInHeader
} from "./styles";
import LogoIMG from "../../../../icons/svg/logo.svg";
import {strings} from "../../../../constants/texts";
import IncognitaIMG from "../../../../icons/svg/faqs.svg"
import TickIMG from "../../../../icons/svg/tick.svg"
import {SwitchIcon, SwitchOffIcon} from '../../../../icons/js/switch'

import BurgerIMG from "../../../../icons/svg/burger.svg"
import SpaguettiIMG from "../../../../icons/svg/pasta.svg"
import SoupIMG from "../../../../icons/svg/soup.svg"
import GuacamoleIMG from "../../../../icons/svg/guacamole.svg"
import CookiesIMG from "../../../../icons/svg/cookies.svg"
import OJuiceIMG from "../../../../icons/svg/orange-juice.svg"
import {InProccess} from "../../../../actions";

const DataOfTheCardByDefault = {
  headerImages: [
    {
      isUrl: false,
      image: SoupIMG,
    },
    {
      isUrl: false,
      image: GuacamoleIMG,
    },
    {
      isUrl: false,
      image: SpaguettiIMG,
    },
  ],
  headerImage10PercOff: {
    isUrl: false,
    image: SpaguettiIMG,
  },
  menuItems: [
    {
      image: BurgerIMG,
      text: "3 comida"
    },
    {
      image: CookiesIMG,
      text: "3 acompañamientos"
    },
    {
      image: OJuiceIMG,
      text: "3 bebidas"
    },
  ],
  bottomButtonText: "Comprar ya - $179 c/u",
  bottomButtonTextOff: "Comprar ya - $149 c/u",
  has10percentageActive: true,
  active10perc: false,
  menuTextWithNOT10PercOff: "distintas",
  menuTextWith10PercOff: "iguales",
  urlWhenPressBuy: "#",
  urlWhenPressBuyWith10PercOff: "#",
};

const Render3EqualFood = (data) => {
  const toRender = [];
  for(let i = 0; i < 3; i++){
    toRender.push(
      <FoodImageInHeader src={data.image} />
    )
  }
  return toRender;
};

export const BigCardHomeLanding = props => {
  const [Is10PercActived, Set10PercActived] = useState(DataOfTheCardByDefault.active10perc);
  const cardColor = props.cardColor ? props.cardColor : colors["normal-primary"];

  return(
    <Container>
      <TopLayout cardColor={cardColor}>
        <ContainerLogoTextAndSwitchInHeader>
          <ContainerLogoText>
            <Logo src={LogoIMG} />
            <NameForLogoInHeader>
              {strings.Home_Name_In_Card_Header}
            </NameForLogoInHeader>
          </ContainerLogoText>
          <ContainerSwitchInHeader onClick={() => Set10PercActived(!Is10PercActived)}>
            <TextSwitchInHeader>
              {strings.Home_Layout1_10PercentageOff}
            </TextSwitchInHeader>
            {
              Is10PercActived
                ? <SwitchIcon color={"white"} size={"3vw"} />
                : <SwitchOffIcon color={"white"} size={"3vw"} />
            }
          </ContainerSwitchInHeader>
        </ContainerLogoTextAndSwitchInHeader>
        <ContainerFoodImagesInHeader>
          {
            Is10PercActived
              ? (Render3EqualFood(DataOfTheCardByDefault.headerImage10PercOff))
              : DataOfTheCardByDefault.headerImages.map((FoodData, index) => {
                return <FoodImageInHeader key={"CardMenuHeaderImages" + index} src={FoodData.image} />
              })
          }
        </ContainerFoodImagesInHeader>
      </TopLayout>
      <MiddleLayout >
        {
          DataOfTheCardByDefault.menuItems.map((data, index) => {
            return (
              <MenuWithImageTickAndTextContainer key={"CardMenuTextItem" + index} >
                <ImageAndTextOfMenu>
                  <ImagesInMenu src={data.image} />
                  <TextInMenu>{data.text}{" "}{
                    DataOfTheCardByDefault.has10percentageActive
                      ? Is10PercActived
                      ? DataOfTheCardByDefault.menuTextWith10PercOff
                      : DataOfTheCardByDefault.menuTextWithNOT10PercOff
                      : DataOfTheCardByDefault.menuTextWithNOT10PercOff
                  }</TextInMenu>
                </ImageAndTextOfMenu>
                <TickOfMenu>
                  <ImagesInMenu src={TickIMG} />

                </TickOfMenu>
              </MenuWithImageTickAndTextContainer>
            )
          })
        }
        <BuyNowButtonInMiddleContainer >
          <BuyNowButtonInMiddleText>{strings.Home_Buy_Now_In_Card}</BuyNowButtonInMiddleText>
          <IncognitaSymbolContainer>
            <IncognitaSymbolInMiddle src={IncognitaIMG} />
          </IncognitaSymbolContainer>
        </BuyNowButtonInMiddleContainer>
      </MiddleLayout>
      <BottomLayout cardColor={cardColor} onClick={InProccess}>
        <ButtonTextInBottom>
          {
            Is10PercActived
              ? DataOfTheCardByDefault.bottomButtonTextOff
              : DataOfTheCardByDefault.bottomButtonText
          }
        </ButtonTextInBottom>
      </BottomLayout>
    </Container>
  )
};
