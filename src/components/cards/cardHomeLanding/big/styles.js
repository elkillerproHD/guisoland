import styled from "styled-components";
import {colors} from "../../../../constants/colors";

export const Container = styled.div `
    width: 30vw;
    -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;
export const TopLayout = styled.div `
    width: 100%;
    padding-bottom: 2vh;
    padding-top: 1vh;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: ${({cardColor})=>cardColor};
`;
export const BottomLayout = styled.div `
    width: 100%;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    background-color: ${({cardColor}) => cardColor};
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    padding-top: 3.8vh;
    padding-bottom: 3.8vh;
    &:hover {
      opacity: 0.7;
    }
  `;
export const MiddleLayout = styled.div `
    width: 100%;
    background-color: white;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    position: relative;
    padding-top: 3vh;
    padding-bottom: 6vh;
  `;
export const Logo = styled.img `
  width: 2.8vw;
  height: 2.8vw;
`;
export const NameForLogoInHeader = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 1.6em;
  font-weight: bold;
  color: white;
  text-align: left;
  margin: 0 0 0 0.4vw;
`;
export const ContainerLogoTextAndSwitchInHeader = styled.div `
  display: flex;
  padding-left: 1.5vw;
  padding-right: 1vw;
  padding-top: 0.5vw;
  align-items: center;
  justify-content: space-between;
`;
export const ContainerLogoText = styled.div `
  display: flex;
  align-items: center;
`;
export const ContainerSwitchInHeader = styled.div `
  display: flex;
  align-items: center;
  cursor: pointer;
`;
export const TextSwitchInHeader = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 15px;
  font-weight: bold;
  color: white;
  margin-right: 0.5vw;
`;
export const SwitchInHeader = styled.img `
  width: 2vw
`;

export const ContainerFoodImagesInHeader = styled.div `
  width: 100%;
  height: 70%;
  margin-top: 1vh;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;
export const FoodImageInHeader = styled.img `
  width: 4.7vw;
  height: 4.7vw;
`;
export const ButtonTextInBottom = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 17px;
  font-weight: bold;
  color: white;
  text-align: center;
`;
export const BuyNowButtonInMiddleContainer = styled.div `
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  position: absolute;
  bottom: 1.4vh;
  left: 0;
`;
export const BuyNowButtonInMiddleText = styled.p `
  width: 40%;
  font-family: 'Montserrat', sans-serif;
  font-size: 12px;
  font-weight: bold;
  color: ${colors.darkgray};
  text-align: center;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;
export const IncognitaSymbolInMiddle = styled.img `
  width: 2vw;
  height: 2vw;
  padding-right: 1.6vw;
  cursor: pointer;
`;
export const IncognitaSymbolContainer = styled.div `
  width: 30%;
  display: flex;
  justify-content: flex-end;
`;
export const MenuWithImageTickAndTextContainer = styled.div `
  width: 95%;
  padding: 2vh 2.5%;
  justify-content: space-around;
  display: flex;
`;
export const ImageAndTextOfMenu = styled.div `
  width: 100%;
  padding-left: 5%;
  justify-content: flex-start;
  align-items: center;
  display: flex;
`;
export const TickOfMenu = styled.div `
  width: 12%;
  display: flex;
  justify-content: flex-end;
`;
export const ImagesInMenu = styled.img `
  width: 2.8vw;
  height: 2.8vw;
`;
export const TextInMenu = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 0.8em;
  font-weight: bold;
  margin: 0 0 0 1.2vw;
  color: ${colors["normal-secondary"]};
`;
