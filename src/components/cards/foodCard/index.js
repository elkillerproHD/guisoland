import React from 'react';
import styled from "styled-components";
import TestImage from '../../../images/recetas/papas_cebolla_crema/imagenes/1.jpg'
import {colors} from "../../../constants/colors";

const Container = styled.div `
  background-color: white;
  width: 18vw;
  border-radius: 12px;
  -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  -moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0vw 1vw 1vh 3vw;
  padding-bottom: 1vh;
`;
const ContainerImage = styled.div `
  width: 18vw;
  height: 10vw;
  overflow: hidden;
`
const Image = styled.img `
  width: 100%;
  border-top-left-radius: 12px;
  border-top-right-radius: 12px;
`;
const FoodName = styled.p `
  font-weight: bold;
  font-family: 'Montserrat';
  color: black;
  font-size: 1em;
  text-align: center;
  padding-left: 5%;
  padding-right: 5%;
  margin-top: 0.5vh;
  word-break: break-all;
  white-space: normal;
`;
const FoodDescription = styled.p `
  font-weight: bold;
  font-family: 'Montserrat';
  color: ${colors.darkgray};
  font-size: 0.8em;
  text-align: center;
  padding-left: 5%;
  padding-right: 5%;
  margin-top: 1vh;
  height: 45px;
  overflow: auto;
`;
const PriceContainer = styled.div `
  width: 86%;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  border: 1px solid black;
  padding: 1vh 2%;
  margin-top: 1.5vh;
`;
const RealPrice = styled.p `
  font-weight: bold;
  font-family: 'Montserrat';
  color: black;
  font-size: 0.8em;
  margin: 0;
`;
const LastPrice = styled.p `
  font-family: 'Montserrat';
  color: ${colors.darkgray};
  font-size: 0.8em;
  text-decoration: line-through;
  margin: 0;
`;
const DescripText = styled.p `
  font-family: 'Montserrat';
  color: ${colors.darkgray};
  font-size: 0.8em;
  margin: 0;
`;
const BuyButton = styled.button `
  background-color: ${colors["normal-primary"]};
  border-radius: 12px;
  width: 90%;
  padding-top: 1vh;
  padding-bottom: 1vh;
  border: none;
  margin-top: 1vh;
  font-size: 1.2em;
  color: white;
  font-weight: bold;
  cursor: pointer;
  outline: none;
  &:hover {
    opacity: 0.8;
  } 
`;

export const FoodCardComponent = props => {
  const { url, test } = props;
  if(test){
    return (
      <Container>
        <Image src={TestImage} />
        <FoodName>Papas, cebolla y crema</FoodName>
        <FoodDescription>No nos hacemos cargo por generar adicción...</FoodDescription>
        <PriceContainer>
          <RealPrice>$140</RealPrice>
          <LastPrice>$140</LastPrice>
          <DescripText>$140</DescripText>
        </PriceContainer>
        <BuyButton>¡Reservar!</BuyButton>
      </Container>
    )
  }
  const { img, name, descrip, lowerPrice, price, smallWord } = props.data;
  const imgSrc = `${url}${img.name}`;
  return (
    <Container>
      <ContainerImage>
        <Image src={imgSrc} />
      </ContainerImage>
      <FoodName>{name}</FoodName>
      <FoodDescription>{descrip}</FoodDescription>
      <PriceContainer>
        <RealPrice>{lowerPrice}</RealPrice>
        <LastPrice>{price}</LastPrice>
        <DescripText>{smallWord}</DescripText>
      </PriceContainer>
      <BuyButton>¡Reservar!</BuyButton>
    </Container>
  )
};
