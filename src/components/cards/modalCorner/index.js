import React from "react";
import styled from "styled-components";
import {colors} from "../../../constants/colors";
import FacebookIcon from '../../../icons/svg/facebook.svg'
import CrossIcon from '../../../icons/svg/close.svg'

const Container = styled.div `
  width: 46vw;
  height: 42vh;
  background-color: white;
  border-radius: 16px;
  -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  -moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  bottom: 1vw;
  left: 1vw;
`;
const Text = styled.p `
  width: 58%;
  padding: 5% 3% 5% 2%;
  font-family: 'Montserrat';
  font-weight: bold;
  font-size: 1.51em;
  color: ${colors.darkgray};
`;
const Image = styled.img `
  width: 12vw;
  height: 12vw;
`;
const Close = styled.img `
  width: 1.5vw;
  height: 1.5vw;
  position: absolute;
  cursor: pointer;
  top: 1vw;
  right: 1vw;
`;
const SubContainer = styled.div `
  display: flex;
  align-items: center;
  position: relative;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const TestText = "¿Sabias que si nos envias una foto con tu pedido y tu usuario, nosotros la compartimos y te damos un descuento del 5%?"

export const ModalCornerAlertComponent = props => {
  return(
    <Container>
      <SubContainer>
        <Text>{TestText}</Text>
        <Image src={FacebookIcon} />
        <Close src={CrossIcon} />
      </SubContainer>
    </Container>
  )
};
