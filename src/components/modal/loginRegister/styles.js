import styled from "styled-components";
import {colors} from "../../../constants/colors";

export const Modal = styled.div `
  width: calc(100vw - 3vw);
  height: calc(100vh - 3vw);
  background-color: rgba(0,0,0,.8);
  position: absolute;
  left: 1.5vw;
  top: 1.5vw;
  margin: auto;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const Card = styled.div `
  width: 40vw;
  height: 88vh;
  background-color: white;
  border-radius: 8px;
  -webkit-box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  -moz-box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  position: relative;
`;
export const Header = styled.div `
 width: 40vw;
 height: 16vh;
 background-color: ${colors["normal-secondary"]};
 border-top-left-radius: 8px;
 border-top-right-radius: 8px;
 display: flex;
 justify-content: space-around;
 align-items: center;
 position: relative;
 flex-direction: column;
`;
export const Logo = styled.img `
  width: 4vw;
  height: 4vw;
`;
export const TextHeader = styled.h3 `
  font-family: 'Montserrat', sans-serif;
  font-size: 20px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 0
`;
export const TextOr = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #c3c3c3;
  margin: 2vh 0;
`;
export const Close = styled.img `
  width: 1.5vw;
  height: 1.5vw;
  &:hover {
    opacity: 0.4;
  }
`;
export const AuthIcon = styled.img `
  width: 4vw;
  height: 4vw;
  &:hover {
    opacity: 0.4;
  }
`;
export const AuthIconContainer = styled.div `
  width: 60%;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  align-items: center;
  justify-content: space-around;
  cursor: pointer;
  margin-top: 3vh;
`;
export const CloseContainer = styled.div `
  width: 2.5vw;
  height: 2.5vw;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  cursor: pointer;
  top: 0.4vw;
  right: 0.6vw;
  background-color: white;
  border-radius: 2.5vw;
  border: 1px solid rgba(0,0,0,0.4);
`;
export const ContainerInputs = styled.div `
  width: 90%;
  margin: auto;
`;
export const EmptyDiv = styled.div `
  width: 100%;
  height: 2vh;
`;
export const Button = styled.button `
  width: 100%;
  height: 10vh;
  position: absolute;
  bottom: 0;
  left: 0;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
  background-color: ${colors["normal-secondary"]};
  border: none;
  color: #fff;
  font-size: 18px;
  font-weight: bold;
  font-family: 'Montserrat';
  cursor: pointer;
  &:hover {
    background-color: ${colors["normal-primary"]};
  }
`;
export const ErrorContainer = styled.div `
  width: 25vw;
  height: 18vh;
  border-radius: 5px;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 2vw;
  right: 1.5vw;
  -webkit-box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  -moz-box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  box-shadow: 0px 2px 5px 0px rgba(255,102,182,1);
  cursor: pointer;
`;
export const TextError = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #ff1a43;
`;
