import React, { useState, useRef } from 'react';
import styled from "styled-components";
import {colors} from "../../../constants/colors";
import LogoIMG from "../../../icons/svg/logo.svg";
import CloseIMG from "../../../icons/svg/close.svg";
// import FacebookIMG from "../../../icons/svg/icon-facebook-square.png";
// import GoogleIMG from "../../../icons/svg/icon-google-square.png";
import {strings} from "../../../constants/texts";
import {MultiSelectorsButton} from "../../buttons/multiSelector";
import { TextField, createMuiTheme, ThemeProvider } from "@material-ui/core";
import {
  // AuthIcon,
  AuthIconContainer, Button,
  Card,
  Close,
  CloseContainer, ContainerInputs, EmptyDiv, ErrorContainer,
  Header,
  Logo,
  Modal, TextError,
  TextHeader,
  TextOr
} from "./styles";

const theme = createMuiTheme({
  palette: {
    common: { black: colors["normal-secondary"], white: colors["normal-secondary"] },
    primary: { main: colors["normal-primary"], dark: colors["normal-secondary"], light: colors["normal-secondary"] },
    text: { primary: colors["normal-secondary"], secondary: colors["normal-secondary"] }
  },
  overrides: {
    MuiInputLabel: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        color: colors["normal-secondary"],
        "&$focused": { // increase the specificity for the pseudo class
          color: colors["normal-primary"]
        }
      },
      underline: {
        "&:before": {
          borderBottom: `1px solid ${colors["normal-secondary"]}`
        }
      }
    }
  }
});
const stringsHere = strings.loginRegisterStrings;
const Buttons = [
  stringsHere.logIn,
  stringsHere.singUp
];
// const AuthIconButtons = [
//   {
//     icon: FacebookIMG,
//     action: () => {}
//   },
//   {
//     icon: GoogleIMG,
//     action: () => {}
//   }
// ];
class LoginRegisterModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loginOrSingUp: 0,
      email: "",
      user: "",
      password: "",
      error: false,
    };
    this.Inputs = [
      {
        value: this.state.email,
        label: "Email",
        name: "email",
      },
      {
        value: this.state.user,
        label: "User",
        name: "user",
      },
      {
        value: this.state.password,
        label: "Password",
        name: "password",
        password: true,
      },
    ];
  }
  SetData = (name, data) => {
    this.setState({[name]: data});
  };
  SetLoginOrSingUpAction = () => {
    const { state } = this;
    if(state.loginOrSingUp === 0){
      this.setState({
        loginOrSingUp: 1
      })
    } else {
      this.setState({
        loginOrSingUp: 0
      })
    }
  };
  PressBottomButton = () => {
    const { email, password, user, error } = this.state;
    function validateEmail(email) {
      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
    function CheckPassword(inputtxt) {
      if(inputtxt.length > 7 && inputtxt.length < 17) {return true;}
      else {return false;}
    }
    if(!validateEmail(email)){
      this.setState({
        error: stringsHere.emailError
      });
    }
    if(!CheckPassword(password)){
      this.setState({
        error: stringsHere.passError
      });
    }
    if(user.length < 1){
      this.setState({
        error: stringsHere.userError
      });
    }
    setTimeout(() => {
      this.setState({
        error: false
      });
    }, 3000)
  };
  render(){
    const { state } = this;

    return(
      <Modal >
        <Card>
          <Header>
            <Logo src={LogoIMG} />
            <TextHeader>{strings.LoginModalHeaderText}</TextHeader>
            <CloseContainer>
              <Close src={CloseIMG} />
            </CloseContainer>
          </Header>
          <div style={{marginTop: 2 + "vh"}} />
          <MultiSelectorsButton onChange={this.SetLoginOrSingUpAction} selected={state.loginOrSingUp} buttons={Buttons} />
          <AuthIconContainer>
            {
              // AuthIconButtons.map((icon) => {
              //   return <AuthIcon src={icon.icon} />
              // })
            }
          </AuthIconContainer>
          <TextOr>{stringsHere.or}</TextOr>
          <ThemeProvider theme={theme}>
            {
              this.Inputs.map((input) => {
                return (
                  <ContainerInputs>
                    <TextField
                      value={state[input.name]}
                      onChange={(e) => {this.SetData(input.name, e.target.value)}}
                      label={input.label}
                      fullWidth
                      type={input.password ? "password" : "text"}
                    />
                  </ContainerInputs>
                )
              })
            }
          </ThemeProvider>
          <Button onClick={this.PressBottomButton}>{stringsHere.logIn}</Button>
        </Card>
        {
          state.error && (
            <ErrorContainer >
              <TextError>{state.error}</TextError>
            </ErrorContainer>
          )
        }
      </Modal>
    )
  }

};

export default LoginRegisterModal;
