import React from 'react';
import LocationIcon from '../../icons/svg/location.svg'
import ShoppingBagIcon from '../../icons/svg/food-bag.svg'
import UserIcon from '../../icons/svg/user.svg'
import ShoppingCartIcon from '../../icons/svg/shopping-cart.svg'
import WhatsAppIcon from '../../icons/svg/whatsapp.svg'
import {colors} from "../../constants/colors";
import styled from "styled-components";
import {
  Link,
} from "react-router-dom";
import {
  Container,
  ContainerLogo,
  ContainerRightIcons,
  Icon,
  NameForLogoInHeader,
  NumberPhoneContainer,
  SeparatorLine
} from "./styles";
import LogoIMG from "../../icons/svg/logo.svg";
import {strings} from "../../constants/texts";
import {OpenCloseButtonWithCalendar} from "../openCloseButtons/openHoursWithCalendar";
import {InProccess} from "../../actions";

const numberPhone = "+54 911 5055-2119";

const rightIcons = [
  {
    icon: LocationIcon,
    width: "3vw",
    height: "3vw",
    hasCircleNumber: false,
    href: "#",
  },
  // {
  //   name: "FoodBag",
  //   icon: ShoppingBagIcon,
  //   width: "3vw",
  //   height: "3vw",
  //   hasCircleNumber: true,
  //   numberOfNotifications: 1,
  //   colorOfCircle: colors.darkgray
  // },
  {
    icon: UserIcon,
    width: "3vw",
    height: "3vw",
    hasCircleNumber: false,
    href: "#",
  },
  {
    name: "ShoppingCart",
    icon: ShoppingCartIcon,
    href: "/buy",
    width: "1.5vw",
    height: "1.5vw",
    hasCircleNumber: true,
    numberOfNotifications: 2,
    colorOfCircle: colors["normal-primary"],
    hasBackgroundColorCircle: true,
    backgroundColorCircle: "white"
  },
];

export default class HeaderComponent extends React.Component {

  constructor(props) {
    super(props);
    let states = {};
    rightIcons.forEach((icons) => {
      if(icons.hasCircleNumber){
        states[icons.name] = icons.numberOfNotifications;
      }
    })
    this.state = {
      appears: false,
      ...states
    };
  }

  RenderIcons = () => {

    const AContainer = styled.div`
      cursor: pointer;
      position: relative;
      border-radius: 3vw;
      width: 3vw;
      height: 3vw;
      display: flex;
      justify-content: center;
      align-items: center;
    `;


    const iconsToRender = [];
    rightIcons.forEach((icon) => {
      const ImgIcon = styled.img `
          width: ${icon.width};
          height: ${icon.height};
          ${AContainer}:hover & {
            -webkit-animation: jello-horizontal 0.9s both;
            animation: jello-horizontal 0.9s both;
          }
      `;
      if(icon.hasBackgroundColorCircle){
        iconsToRender.push(
          <Link to={icon.href}>
            <AContainer style={{ backgroundColor: "white" }} >
              {icon.hasCircleNumber && (
                <div style={{position: 'absolute', bottom: -0.3 + "vw", left: -0.3 + "vw", backgroundColor: icon.colorOfCircle, borderRadius: 3 + "vw", width: 1.4 + "vw", height: 1.4 + "vw", display: "flex",justifyContent: "center", alignItems: "center"}}>
                  <p style={{color: "white", fontSize: 0.6 + "em", fontFamily: 'Montserrat', fontWeight: 'bold'}}>{icon.numberOfNotifications}</p>
                </div>
              )}
              <ImgIcon src={icon.icon} />
            </AContainer>
          </Link>
        )
      } else {
        iconsToRender.push(
          <Link to={icon.href}>
            <AContainer style={{cursor: "pointer", position: "relative"}}>
              {icon.hasCircleNumber && (
                <div style={{position: 'absolute', bottom: -0.3 + "vw", left: -0.3 + "vw", backgroundColor: icon.colorOfCircle, borderRadius: 3 + "vw", width: 1.4 + "vw", height: 1.4 + "vw", display: "flex",justifyContent: "center", alignItems: "center"}}>
                  <p style={{color: "white", fontSize: 0.6 + "em", fontFamily: 'Montserrat', fontWeight: 'bold'}}>{icon.numberOfNotifications}</p>
                </div>
              )}
              <ImgIcon src={icon.icon} />
            </AContainer>
          </Link>

        )
      }
    })
    return iconsToRender;
  };
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    if (window.pageYOffset > window.innerHeight * 0.1) {
      this.setState({
        appears: true
      });
    } else {
      this.setState({
        appears: false
      });
    }
  };

  render () {
    const { state } = this;
    if(!state.appears){
      return null
    }
    return(
      <Container>
        <OpenCloseButtonWithCalendar />
        <SeparatorLine />
        <NumberPhoneContainer target="_blank" href={'https://wa.me/5491150552119'}>
          <Icon src={WhatsAppIcon} />
          <NameForLogoInHeader>
            {numberPhone}
          </NameForLogoInHeader>
        </NumberPhoneContainer>
        <ContainerLogo>
          <Icon src={LogoIMG} />
          <NameForLogoInHeader>
            {strings.Home_Name_In_Card_Header}
          </NameForLogoInHeader>
        </ContainerLogo>
        <ContainerRightIcons>
          {this.RenderIcons()}
        </ContainerRightIcons>
      </Container>
    )
  }
};
