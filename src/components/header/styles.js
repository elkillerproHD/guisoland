import styled from "styled-components";
import {colors} from "../../constants/colors";

export const Container = styled.div `
  width: 100vw;
  height: 10vh;
  background-color: rgba(255,178,199,1);
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 3;
  overflow: hidden;
  webkit-animation: scale-up-ver-top 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  animation: scale-up-ver-top 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;

`;
export const ContainerRightIcons = styled.div `
  width: 30vw;
  height: 15vh;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 4vw;
`;


export const ContainerLogo = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  padding-left: 2vw;
  padding-right: 2vw;
`;
export const NumberPhoneContainer = styled.a `
  display: flex;
  padding-left: 2vw;
  padding-right: 2vw;
  align-items: center;
  text-decoration: none;
`;
export const Icon = styled.img `
  width: 3.2vw;
  height: 3.2vw;
  ${NumberPhoneContainer}:hover & {
    background-color: ${colors["normal-primary"]};
    padding: 0.5vw;
    margin: -0.5vw;
    border-radius: 10vw;
  }
`;
export const NameForLogoInHeader = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 22px;
  font-weight: bold;
  color: white;
  text-align: left;
  margin-left: 0.4vw;
  ${NumberPhoneContainer}:hover & {
    text-decoration: underline;
  }
`;
export const SeparatorLine = styled.div `
  width: 2px;
  height: 8vh;
  background-color: white;
  margin-left: 1vw;
  margin-right: 1vw;
`;
