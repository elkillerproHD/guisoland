import React from 'react';
import styled from "styled-components";
import {colors} from "../../constants/colors";
import SearchIcon from '../../icons/svg/search.svg'
import {generateKey} from "../../functions/key";

const ButtonFilter = styled.div `
  padding-left: 1vw;
  padding-right: 1vw;
  margin-left: 1vw;
  margin-right: 1vw;
  height: 6.5vh;
  border-radius: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: none;
 
  &:hover {
    opacity: 0.9;
    -webkit-animation: jello-horizontal 0.9s both;
    animation: jello-horizontal 0.9s both;
  }
`;
const ButtonFilterText = styled.p `
  font-family: 'Montserrat';
  color: white;
  font-size: 1.2em;
  font-weight: bold;
`;
const Container = styled.div `
  width: 100vw;
  height: 9vh;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const SearchInputContainer = styled.div `
   width: 22vw;
   height: 6.5vh;
   margin-left: 2vw;
   padding-left: 3vw;
   border-bottom: 1px solid ${colors.darkgray};
   position: relative;
`;
const SearchInput = styled.input `
  width: 22vw;
  height: 5.5vh;
  padding-top: 0.4vh;
  margin-left: 0.5vw;
  border: none;
  font-family: 'Montserrat';
  font-weight: bold;
  font-size: 1.2em;
  outline: none;
`;
const SearchIconContainer = styled.img `
  width: 1.5vw;
  height: 1.5vw;
  position: absolute;
  left: 0.1vw;
  bottom: 0.5vw;
`;

const filters = [
  "Promos",
  "Ensaladas",
  "Sandwitches",
  "Pastas",
  "Exquisitos",
];

const RenderButtons = () => {
  const toRender = [];
  let number = 0;
  filters.forEach((filter, index) => {
    if(number === 0){
      toRender.push(
        <ButtonFilter key={generateKey(`RenderButtons${index}`)} style={{ backgroundColor: colors["normal-secondary"] }}>
          <ButtonFilterText>
            {filter}
          </ButtonFilterText>
        </ButtonFilter>
      );
      number++;
    } else {
      if(number === 1){
        toRender.push(
          <ButtonFilter key={generateKey(`RenderButtons${index}`)} style={{ backgroundColor: colors["normal-primary"] }}>
            <ButtonFilterText>
              {filter}
            </ButtonFilterText>
          </ButtonFilter>
        );
        number++;
      } else {
        toRender.push(
          <ButtonFilter key={generateKey(`RenderButtons${index}`)} style={{ backgroundColor: colors.darkgray }}>
            <ButtonFilterText>
              {filter}
            </ButtonFilterText>
          </ButtonFilter>
        );
        number = 0;
      }
    }
  });
  return toRender;
};


export const MenusFiltersComponent = props => {
  return(
    <Container>
      {RenderButtons()}
      <SearchInputContainer >
        <SearchIconContainer src={SearchIcon} />
        <SearchInput />
      </SearchInputContainer>
    </Container>
  )
};
