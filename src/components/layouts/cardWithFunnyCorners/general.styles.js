import styled from "styled-components";

export const CardStyled = styled.div `
  width: 95vw;
  height: 83vh;
  margin: auto;
  -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  -moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  border-radius: 8px;
  position: relative;
  background-color: white;
`;

export const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-content: center;
`;


