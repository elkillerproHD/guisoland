import React from 'react';
import styled from "styled-components";
import Corner from '../../../icons/svg/triangulo.svg'
import CornerRotated from '../../../icons/svg/triangulo.rotated.svg'
// import Menu from '../../../icons/svg/menu.svg'
// import {colors} from "../../../constants/colors";

const CornerTopRightContainer = styled.div `
    width: 9.5vw;
    height: 9.5vw;
    position: absolute;
    top: -0.6vw;
    right: -0.6vw;
  `;
const CornerTopRightSubContainer = styled.div `
    justify-content: center;
    align-items: center;
    position: relative;
    width: 9.5vw;
    height: 9.5vw;
  `;
const CornerTopRightCornerS = styled.img `
    width: 8.5vw;
    height: 7.5vw;
    fill: none;
    stroke: #646464;
    stroke-width: 2px;
    stroke-dasharray: 2,2;
    stroke-linejoin: round;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
    cursor: pointer;
  `;
// const CornerTopRightMenuS = styled.img `
//     width: 2vw;
//     height: 2vw;
//     position: absolute;
//     margin-left: auto;
//     margin-right: auto;
//     left: 65%;
//     right: 45%;
//     top: 15%;
//     z-index: 2;
//     fill: ${colors.white};
//     cursor: pointer;
//   `;

export const CornerTopRight = props => {

  console.log(props)
  return (
    <CornerTopRightContainer onClick={(e) => props.onClick(e)}>
      <CornerTopRightSubContainer>
        {/*<CornerTopRightMenuS src={Menu} />*/}
        <CornerTopRightCornerS src={Corner} />
      </CornerTopRightSubContainer>
    </CornerTopRightContainer>
  )
};

const CornerBottomLeftContainer = styled.div `
    width: 9.5vw;
    height: 9.5vw;
    position: absolute;
    bottom: -0.6vw;
    left: -0.6vw;
  `;
const CornerBottomLeftSubContainer = styled.div `
    justify-content: center;
    align-items: center;
    position: relative;
    width: 9.5vw;
    height: 9.5vw;
  `;
const CornerBottomLeftCornerS = styled.img `
    width: 8.5vw;
    height: 7.5vw;
    fill: none;
    stroke: #646464;
    stroke-width: 2px;
    stroke-dasharray: 2,2;
    stroke-linejoin: round;
    position: absolute;
    bottom: 0;
    left: 0;
    z-index: 1;
    cursor: pointer;
  `;

export const CornerBottomLeft = () => {
  return (
    <CornerBottomLeftContainer>
      <CornerBottomLeftSubContainer>
        <CornerBottomLeftCornerS src={CornerRotated} />
      </CornerBottomLeftSubContainer>
    </CornerBottomLeftContainer>
  )
};
