import React from 'react'
import {strings} from "../../../constants/texts";
import {Header, LogoHeader, TextHeader} from "./header.styles";
import {CardStyled, Container} from "./general.styles";
import Logo from "../../../icons/svg/logo.svg";
import {CornerBottomLeft, CornerTopRight} from "./corners.styles";

export const CardWithFunnyCorners = props => {
  return (
    <Container >
      <CardStyled >
        <Header >
          <LogoHeader src={Logo} />
          <TextHeader>{strings.Home_Layout1_name}</TextHeader>
        </Header>
        {props.children}
        <CornerTopRight />
        {/*<CornerTopRight onClick={ScrollAction} />*/}
        <CornerBottomLeft />
      </CardStyled>
    </Container>
  )
};
