import styled from "styled-components";
import {colors} from "../../../constants/colors";

export const Header = styled.div `
  width: 94vw;
  height: 11%            ;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  background-color: ${colors["normal-primary"]};
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding-left: 1%;
`;

export const LogoHeader = styled.img `
  width: 4.5vw;
  height: 80%;
`;

export const TextHeader = styled.h1 `
  font-family: 'Montserrat', sans-serif;
  font-size: 22px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin-left: 1%;
`;

