import React from 'react';
import styled from "styled-components";
import {colors} from "../../../constants/colors";
import {strings} from "../../../constants/texts";

const todayData = {
  timeText: "8:00 AM - 22:00 PM",
  startDate: 8 * 60,
  endDate: 22 * 60,
  todayIsClosed: false,
  whyIsClose: "",
};

const Container = styled.a `
  width: 14vw;
  padding-left: 1vw;
  padding-right: 0.2vw;
  cursor: pointer;
  text-decoration: none;
`;
const RounderSquare = styled.div `
  border-radius: 12px;
  background-color: white;
  width: 13.4vw;
  height: 3.5vh;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 0.7vh;
  padding-bottom: 0.7vh;
`;
const TextOfOpenClose = styled.p `
  font-family: 'Montserrat', sans-serif;
  color: ${colors["normal-primary"]};
  font-size: 1em;
  font-weight: bold;
  text-decoration: none;
`;
const TextTime = styled.p `
  font-family: 'Montserrat', sans-serif;
  color: white;
  width: 13.4vw;
  font-size: 0.8em;
  text-align: center;
  font-weight: bold;
  white-space: nowrap;
  text-decoration: none;
`;

export const OpenCloseButtonWithCalendar = props => {
  const todayTime = new Date();
  let isOpen = false;
  if((todayTime.getHours() + 1) * 60 >= todayData.startDate && (todayTime.getHours() + 1) * 60 <= todayData.endDate){
    isOpen = true;
  }
  return (
    <Container href={'#'}>
      <TextTime>{todayData.timeText}</TextTime>
      <RounderSquare>
        <TextOfOpenClose>{isOpen ? strings.Open_SingBoard : strings.Close_SingBoard}</TextOfOpenClose>
      </RounderSquare>
    </Container>
  )
};
