import React from 'react';
import styled from "styled-components";
import {colors} from "../../constants/colors";
import {strings} from "../../constants/texts";

const Container = styled.div `
  height: 100vh;
  width: 97vw;
  padding-left: 1vw;
  padding-right: 1vw;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${colors["normal-secondary"]};
`;

const SubContainer = styled.div `
  height: 97vh;
  width: 98vw;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: white;
  border-radius: 12px;
  overflow: hidden;
`;
const Text = styled.p `
  color: ${colors["normal-secondary"]};
  font-size: 3em;
  width: 90vw;
  font-family: 'Montserrat';
  font-weight: bold;
  text-align: left;
  padding-left: 0%;
  margin-bottom: 1vh;
`;

export const MapForLanding = props => {

  return (
    // Important! Always set the container height explicitly
    <Container >
      <SubContainer >
        <Text>{strings.WeAreHere}</Text>
        <iframe
          title="IframeMap"
  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26287.020735216207!2d-58.554863152247975!3d-34.55665455017156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcb7441d15d533%3A0x23569c069187a3da!2sSan%20Andres%2C%20Buenos%20Aires!5e0!3m2!1ses!2sar!4v1581090839784!5m2!1ses!2sar"
  width={window.innerWidth * 0.9} height={window.innerHeight * 0.8} frameBorder="0" style={{border: 0}} allowFullScreen=""/>
      </SubContainer>
    </Container>
  )
};
MapForLanding.defaultProps = {
  center: {
    lat: 59.95,
    lng: 30.33
  },
  zoom: 11
};
