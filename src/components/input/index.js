/* eslint-disable react/destructuring-assignment,react/prop-types,no-nested-ternary */
import React, { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';
import EyeIcon from '../../icons/js/eye.svg';
import {colors} from "../../constants/colors";

const { useRef } = require('react');

const InputHighlighter = (color) => keyframes` {
      from { background: ${color}; }
      to { width:0; background:transparent; }
    } `;
const Group = styled.div`
  position:relative;
  margin-top: 4vh;
  width: 100%;
`;
const InputS = styled.input`
&&& {
  color: ${({ textColor }) => textColor};
  border: none;
  border-bottom: 1px solid ${({ underlineColor }) => underlineColor};
  font-size:18px;
  padding:10px 0px 3px 0px;
  display:block;
  width: 100%;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",sans-serif;
  background-color: transparent;
  outline:none;
  opacity: .8;
  text-align: ${({ textAlign }) => textAlign};
  ${({leftIconPadding}) => { if(!!leftIconPadding){return `
    width: 95%;
    padding-left: 5%;
  `} }}
  & :focus {
    opacity: 1;
  }
}
  
`;
const Label = styled.label`
  color:${({ color }) => color};
  font-size:18px;
  font-weight:normal;
  position:absolute;
  pointer-events:none;
  width: 100%; 
  top:10px;
  width: 100%;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",sans-serif;
  transition:0.2s ease all;
  -moz-transition:0.2s ease all;
  -webkit-transition:0.2s ease all;
  opacity: .8;
  ${InputS}:focus ~ & , ${InputS}:valid ~ &  {
    top: -20px;
    left: 0;
    font-size:14px;
  }

  ${({ textAlign, leftIconPadding }) => {
    if (textAlign === 'left') {
      if(!!leftIconPadding){
        return `
          left: 4%;
          text-align: left;
        `;
      } else {
        return `
          left:5px;
          text-align: left;
        `;
      }
      
    } if (textAlign === 'center') {
      return `
        left:0px;
        text-align: center;
      `;
    } if (textAlign === 'right') {
      return `
        right:5px;
        text-align: right;
      `;
    }
    return false;
  }}
`;
const Bar = styled.span`
  position:relative;
  display:block;
  width:100%;
  &:before, &:after {
    content:'';
    height:1px;
    width:0;
    bottom:1px;
    position:absolute;
    background:${({ color }) => color};
    transition:0.2s ease all;
    -moz-transition:0.2s ease all;
    -webkit-transition:0.2s ease all;
  }
  &:before {
    left:50%;
  }
  &:after {
    right:50%;
  }
  ${InputS}:focus ~ &:before, ${InputS}:focus ~ &:after {
    width:50%;
  }
`;
const Highlight = styled.span`
  position:absolute;
  height:60%;
  width:100px;
  top:25%;
  left:0;
  pointer-events:none;
  opacity:0.5;
  ${InputS}:focus ~ & {
    -webkit-animation:${({ color }) => InputHighlighter(color)} 0.3s ease;
    -moz-animation:${({ color }) => InputHighlighter(color)} 0.3s ease;
    animation:${({ color }) => InputHighlighter(color)} 0.3s ease;
  }
`;
const EyeComponent = styled.span`
  width: 2vw;
  height: 1vh;
  position: absolute;
  right: 0.1vw;
  bottom: 2vh;
  top: 0;
  margin: auto;
  cursor: pointer;
`;

const LeftIconContainer = styled.span `
  height: 1vh;
  position: absolute;
  left: 0.1vw;
  bottom: 3vh;
  top: 0;
  margin: auto;
  cursor: pointer;
`

// eslint-disable-next-line import/prefer-default-export
export const Input = (props) => {
  const [secureEntry, setSecureEntry] = useState(true);

  return (
    <Group>
        {
          props.leftIcon && (
            <LeftIconContainer>
              { props.leftIcon }
            </LeftIconContainer>
          )
        }
        <InputS
          leftIconPadding={props.leftIcon}
          textColor={props.inputTextColor}
          underlineColor={props.underlineInputColor}
          value={props.value}
          onChange={props.onChange}
          type={props.password ? secureEntry ? 'password' : 'text' : 'text'}
          textAlign={props.textInputAlign}
          required
        />

      <Bar color={props.underlineExpansibleBarColor} />
      <Highlight color={props.highlightColor} />

      {
        props.labelIsValue
          ? props.value !== undefined
            ? (
              <Label leftIconPadding={props.leftIcon} textAlign={props.textLabelAlign} color={props.labelTextColor}>{props.value}</Label>
            )
            : (
              <Label leftIconPadding={props.leftIcon} textAlign={props.textLabelAlign} color={props.labelTextColor}>{props.text}</Label>
            )
          : (
            <Label leftIconPadding={props.leftIcon} textAlign={props.textLabelAlign} color={props.labelTextColor}>{props.text}</Label>
          )
      }
      {
        props.password && (
          <EyeComponent onClick={() => { setSecureEntry(!secureEntry); }}>
            <EyeIcon fill={props.inputTextColor} />
          </EyeComponent>
        )
      }
    </Group>
  );
};
Input.defaultProps = {
  text: 'Label',
  onChange: () => {},
  value: undefined,
  labelIsValue: false,
  leftIcon: false,
  underlineInputColor: colors["normal-secondary"],
  underlineExpansibleBarColor: colors["normal-secondary"],
  inputTextColor: colors["normal-primary"],
  labelTextColor: colors["normal-primary"],
  highlightColor: colors["normal-primary"],
  textInputAlign: 'left',
  textLabelAlign: 'left',
  password: true,
};
