/* eslint-disable react/destructuring-assignment,react/prop-types,jsx-a11y/alt-text */
import React from 'react';
import styled from 'styled-components';

const strings = {
  textOfAvailableFormats: 'Solo formatos jpg | png',
  pickImage: 'Seleccione una imagen',
};

const Text = styled.p`
  font-size: 12px;
  color: white;
  font-family: "Roboto";
  text-align: center;
  margin-left: auto;
  margin-right: auto;
`;

// Yes sexy button, do you have a problem?
const SexyUploadButton = styled.button`
  background: #B13E6A;
  color: white;
  font-weight: 300;
  font-size: 14px;
  margin: 10px 0;
  transition: all 0.2s ease-in;
  cursor: pointer;
  outline: none;
  border-color: #FFFBFB;
  border-width: 1px;
  border-style: solid;
  width:18vw;
  height: 8vh;
  min-width: 200px;
  &:hover {
    color: #B13E6A;
    background: white;
    width:19vw;
    height: 8vh;
    min-width: 220px;
  }
`;
const ImgContainer = styled.div`
  width: 25%;
  margin: 5%;
  padding: 10px;
  background: #edf2f6;
  display: flex;
  align-items: center;
  justify-content: center;
  height: inherit;
  box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.1);
  border: 1px solid #d0dbe4;
  position: relative;
`;
const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  flex-direction: column;
`;
const CrossIcon = (props) => {
  const Circle = styled.div`
    position: absolute;
    top: -9px;
    right: -9px;
    color: #fff;
    background: #F44237;
    text-align: center;
    cursor: pointer;
    font-size: 20px;
    font-weight: bold;
    line-height: 30px;
    width: 30px;
    height: 30px;
    border-radius: 100%;
`;
  return <Circle onClick={props.onClick}>X</Circle>;
};
class Upload extends React.Component {
  // eslint-disable-next-line react/static-property-placement
  static defaultProps = {
    handleChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      file: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    const { props } = this;
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
    });
    props.handleChange(event.target.files[0]);
  };

  removeImage = () => {
    const { props } = this;
    this.setState({ file: null });
    props.handleChange(undefined);
  };

  render() {
    return (
      <Container>
        <Text>{strings.textOfAvailableFormats}</Text>
        {/* eslint-disable-next-line no-return-assign */}
        <input style={{ display: 'none' }} ref={(fileInput) => this.fileInput = fileInput} type="file" onChange={this.handleChange} />
        <SexyUploadButton onClick={() => this.fileInput.click()}>{strings.pickImage}</SexyUploadButton>
        {
          this.state.file && (
            <ImgContainer>
              <img style={{ width: '100%' }} src={this.state.file} />
              <CrossIcon onClick={this.removeImage} />
            </ImgContainer>
          )
        }

      </Container>
    );
  }
}
export default Upload;
