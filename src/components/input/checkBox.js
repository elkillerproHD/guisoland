/* eslint-disable react/destructuring-assignment,react/prop-types */
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 95%;
  height: 1.1vw;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-top: 3vh;
  padding-left: 5%;
  cursor: pointer;
`;
const Square = styled.div`
  width: 1.2vw;
  height: 1.1vw;
  background-color: transparent;
  border: 2px solid #212c4c;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const SquareSelected = styled.div`
  width: 0.8vw;
  height: 0.8vw;
  margin-bottom: 1px;
  background-color: #212c4c;
`;
const Text = styled.p`
  color: rgba(0,0,0,.26);
  font-size: 14px;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  margin: 0 0 0 1vw;
`;
// eslint-disable-next-line import/prefer-default-export
export const CheckBox = (props) => (
  <Container onClick={props.onClick}>
    <Square>
      {
          props.selected && <SquareSelected />
        }
    </Square>
    <Text>{props.text}</Text>
  </Container>
);
CheckBox.defaultProps = {
  selected: true,
  onClick: () => {},
  text: 'Texto',
};
