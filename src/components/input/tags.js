/* eslint-disable react/prop-types,react/destructuring-assignment */
import React from 'react';
import styled from 'styled-components';
import { GenerateKey } from '../../utils/generateKey';

const Container = styled.div`
  padding: 5px 20px;
  transition: all 0.2s ease;
  cursor: pointer;
  opacity: 0.8;
  color: #212c4c;
  background-color: white;
  margin: 5px;
  display: inline-block;
  padding: 5px 20px;
  position: relative;
  font-size: .8rem; 
  font-family: "Roboto", "Helvetica", "Arial", sans-serif ;
  &:hover {
    opacity: 1;
  }
`;
const Cross = styled.span`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif ;
  font-size: .8rem; 
  color: #212c4c;
  right: 5px;
  top: auto;
  bottom: auto;
  margin: auto;
  position: absolute;
`;

// eslint-disable-next-line no-unused-vars
export const Tag = (props, index) => (
  <Container onClick={props.onClick}>
    {props.children}
    {props.canClose && (<Cross>X</Cross>)}
  </Container>
);

export default class TagStorm extends React.Component {
  constructor(props) {
    super(props);
    const availableTag = props.defaultTags.map((item, index) => ({ value: item, index }));
    this.state = {
      renderTags: null,
      availableTag,
    };
  }

  // eslint-disable-next-line react/sort-comp
  RenderTagFunction = () => {
    // Colocamos primero las tags que estan en la props selected
    const { state, props } = this;
    const GetTag = [];
    props.selected.forEach((s, index) => {
      GetTag.push(<Tag key={GenerateKey(index)} canClose onClick={() => { this.OnRemove(s); }}>{state.availableTag[s].value}</Tag>);
    });
    // Luego de colocar las seleccionadas se pushean las no seleccionadas
    if (props.search !== undefined) {
      const likeSearch = state.availableTag.filter((item) => item.value.toLowerCase().indexOf(props.search.toLowerCase()) > -1);
      likeSearch.forEach((tag, index) => {
        if (props.selected.indexOf(tag.index) === -1) {
          GetTag.push(<Tag key={GenerateKey(index)} onClick={() => { this.OnSelect(tag.index); }}>{tag.value}</Tag>);
        }
      });
    } else if (state.availableTag.length > 0) {
      state.availableTag.forEach((tag, index) => {
        if (props.selected.indexOf(tag.index) === -1) {
          GetTag.push(<Tag key={GenerateKey(index)} onClick={() => { props.OnSelect(tag.index); }}>{tag.value}</Tag>);
        }
      });
    }

    this.setState({ renderTags: GetTag });
  };

  componentDidMount() {
    this.RenderTagFunction();
  }

  componentDidUpdate(prevProps) {
    const { props } = this;
    if (prevProps.search !== props.search || prevProps.selected !== props.selected) {
      const availableTag = props.defaultTags.map((item, index) => ({ value: item, index }));
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        availableTag,
      });
      this.RenderTagFunction();
    }
  }

  OnSelect = (index) => {
    const { props } = this;
    if (props.selected.indexOf(index) === -1) {
      props.OnSelect(props.name, index);
    }
    setTimeout(this.RenderTagFunction, 200);
  };

  OnRemove = (index) => {
    const { props } = this;
    props.OnRemove(props.name, index);
    setTimeout(this.RenderTagFunction, 200);
  };

  render() {
    const { state } = this;
    return state.renderTags;
  }
}
TagStorm.defaultProps = {
  defaultTags: ['Matematicas', 'Literatura', 'Sociales', 'Naturales'],
  selected: [],
  search: '',
  name: '',
  OnSelect: () => {},
  OnRemove: () => {},
};
