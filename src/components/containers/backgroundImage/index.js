import React from 'react';
import styled from 'styled-components';
//import '@storybook/addon-console';
import BackgroundTestImage from './backgroundTestImage.jpg'

// const consoleText =
//   [
//     "You can pass a property \"color\". This property will be used for the background color of the modal, if you do not pass color the color of the modal will be rgba(0,0,0,0.4)",
//     "You can pass a property \"modal\". It's by default true, if you pass false, the modal would not be rendered",
//     "If you do not pass a size value ( width || height ) it will has the window size",
//     "This is a container component.",
//     "Properties: image, color, width, height",
//   ];

let Wrapper = styled.div`
    width: 100vw;
    height: 100vh;
    position: relative;
    overflow: hidden;
  `;

let Modal = styled.div`
    background-color: ${({color}) => color};
    width: 100vw;
    height: 100vh;
    position: absolute;
    bottom: 0;
    left: 0;
    z-index: 1;
  `;

let ImageBackground = styled.img`
    width: ${({width}) => width};
    height: ${({height}) => height};
  `;

export const BackgroundImage = (props) => {
  // consoleText.forEach(text => console.log(text));
  const color = props.color ? props.color : 'rgba(0,0,0,0.4)';
  const image = props.image ? props.image : BackgroundTestImage;
  const modal = props.modal === false ? false : true;
  const width = props.width ? props.width : "100vw";
  const height = props.height ? props.height : "100vh";




// Use them like any other React component – except they're styled!
  return(
    <Wrapper>
      <ImageBackground width={width} height={height} src={image} />
      {
        modal &&
        (
          <Modal color={color} >
            {props.children}
          </Modal>
        )
      }
    </Wrapper>
  )
};
