import React from 'react';
import styled from 'styled-components';
//import '@storybook/addon-console';

// const consoleText =
//   [
//     "You can pass a a property \"color\". This property will be used for the background color",
//     "If you do not pass a size value ( width || height ) it will has the window size",
//     "This is a container component.",
//     "Properties: color, width, height, hasTestorder",
//   ];

let Wrapper = styled.div`
  background-color: ${({color}) => color};
  width: ${({width}) => width};
  height: ${({height}) => height};
  ${({hasTestBorder}) => {if(hasTestBorder){return 'border: 2px solid red'}}};
  overflow: hidden;
`;

export const Background = (props) => {
  // consoleText.forEach(text => console.log(text));
  const color = props.color ? props.color : 'transparent';
  const width = props.width ? props.width : "100vw";
  const height = props.height ? props.height : "100vh";
  const hasTestBorder = props.hasTestBorder;

// Use them like any other React component – except they're styled!
  return(
    <Wrapper
      color={color}
      width={width}
      height={height}
      hasTestBorder={hasTestBorder}
    >
      {props.children}
    </Wrapper>
  )
};
