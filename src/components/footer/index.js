import React from 'react'
import styled from "styled-components";
import {colors} from "../../constants/colors";
// import TwitterIMG from '../../icons/svg/twitter-white.svg';
import FacebookIMG from '../../icons/svg/facebook-white.svg';
import InstagramIMG from '../../icons/svg/instagram-white.svg';
import {SocialNetworksLinks} from '../../constants/socialNetworks'
import {strings} from "../../constants/texts";
import {InProccess} from "../../actions";
import {generateKey} from "../../functions/key";

const Container = styled.div `
  width: 97vw;
  padding: 0.6vw 1vw;
  height: 46vh;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const SubContainer = styled.div`
  width: 99vw;
  height: 44vh;
  background-color: ${colors["normal-secondary"]};
  border-radius: 12px;
  -webkit-box-shadow: 0 1px 7px 0 rgba(0,0,0,0.5);
  -moz-box-shadow: 0 1px 7px 0 rgba(0,0,0,0.5);
  box-shadow: 0 1px 7px 0 rgba(0,0,0,0.5);
  display: flex;
  justify-content: space-between;
`;
const Columns = styled.div `
  width: 40%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;
const ColumnRight = styled.div `
  width: 40%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const ContainerDataTexts = styled.div `
  padding-top: 3vh;
  padding-left: 2vw;
  display: flex;
  flex-direction: column;
`;
const ContainerSocialIcons = styled.div `
  display: flex;
  padding-top: 3vh;
  padding-left: 2vw;
  margin-top: 3vh;
`;
const ImageComponent = styled.img `
  width: 5vw;
  height: 5vw;
  margin-right: 2vw;
  a:hover & {
    -webkit-animation: jello-horizontal 0.9s both;
    animation: jello-horizontal 0.9s both;
  }
`;
const PrivacyPolicyContainer = styled.div `
  display: flex;
  margin-left: 2vw;
  margin-top: 4vh;
`;
const PrivacyPolicyText = styled.p `
  font-family: 'Montserrat',serif;
  color: white;
  font-weight: bold;
  font-size: 1em;
`;
const FooterLinePrivacyPolicy = styled.div `
  width: 1px;
  background-color: white;
  height: inherit;
  margin-left: 0.5vw;
  margin-right: 0.5vw;
`;
const PrivacyPolicyLink = styled.a `
  font-family: 'Montserrat',serif;
  color: white;
  font-size: 1em;
  cursor: pointer;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`;
const ContainerFunnyText = styled.div `
  display: flex;
  flex-direction: column;
  padding-top: 2vw;
  padding-right: 2vw;
`;
const FunnyTexts = styled.p `
  font-size: 1.4em;
  font-weight: bold;
  color: white;
  font-family: 'Montserrat',serif;
`;
const HashtagText = styled.p `
  font-size: 1.4em;
  font-weight: bold;
  color: black;
  font-family: 'Montserrat',serif;
`;
const ContainerHastags = styled.div `
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  padding-right: 1vw;
  margin-top: 5vh;
`;
const BoldText = styled.p `
    font-size: 1.2em;
    font-weight: bold;
    color: white;
    font-family: 'Montserrat',serif;
  `;
const Text = styled.p `
    font-size: 1.2em;
    color: white;
    font-family: 'Montserrat',serif;
    margin-left: 1vw;
  `;
const DataTextContainer = styled.div `
    display: flex;
  `;
const DataText =  props => {
  return(
    <DataTextContainer>
      <BoldText>{props.bold}</BoldText>
      <Text>{props.text}</Text>
    </DataTextContainer>
  )
};

const socialConstants = [
  {
    icon: FacebookIMG,
    link: SocialNetworksLinks.facebook
  },
  // {
  //   icon: TwitterIMG,
  //   link: SocialNetworksLinks.twitter
  // },
  {
    icon: InstagramIMG,
    link: SocialNetworksLinks.instagram
  },
];
const contactData = [
  {
    bold: strings.FooterData.telephone.text,
    text: strings.FooterData.telephone.data,
  },
  {
    bold: strings.FooterData.email.text,
    text: strings.FooterData.email.data,
  },
];

export const FooterComponent = () => {
  return (
    <Container>
      <SubContainer>
        <Columns>
          <ContainerDataTexts>
            {
              contactData.map((data, index) => (
                <DataText key={generateKey(`DataTextFooterComponent${index}`)} bold={data.bold} text={data.text} />
              ))
            }
          </ContainerDataTexts>
          <ContainerSocialIcons>
            {
              socialConstants.map((data, index) => (
                <a key={generateKey(`socialConstantsFooterComponent${index}`)} target="_blank" rel="noopener noreferrer" href={data.link}>
                  <ImageComponent src={data.icon} />
                </a>
              ))
            }
          </ContainerSocialIcons>
          <PrivacyPolicyContainer>
            <PrivacyPolicyText>{strings.PrivacyDataText}</PrivacyPolicyText>
            <FooterLinePrivacyPolicy />
            <PrivacyPolicyLink onClick={InProccess}>{strings.PrivacyDataTextLink}</PrivacyPolicyLink>
          </PrivacyPolicyContainer>
        </Columns>
        <ColumnRight>
          <ContainerFunnyText>
            <FunnyTexts>{strings.FooterFunnyText1}</FunnyTexts>
            <FunnyTexts>{strings.FooterFunnyText2}</FunnyTexts>
          </ContainerFunnyText>
          <ContainerHastags>
            {
              strings.Hashtags.map((text, index)=>(
                <HashtagText key={generateKey(`HashtagTextFooterComponent${index}`)}>{text}</HashtagText>
              ))
            }
          </ContainerHastags>
        </ColumnRight>
      </SubContainer>
    </Container>
  );
};
