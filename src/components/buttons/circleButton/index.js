import React from 'react'
import styled from "styled-components";
import {colors} from "../../../constants/colors";

const Container = styled.div `
  width: 10vw;
  height: 10vw;
  min-width: 50px;
  min-height: 50px;
  border-radius: 10vw;
  background-color: ${colors["normal-primary"]};
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.63);
  opacity: 1;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  &:hover {
    opacity: 0.6;
  }
`

export const CircleButton = (props) => {
  return(
    <Container onClick={props.onClick}>
      { props.children }
    </Container>
  )
}
CircleButton.defaultProps = {
  onClick: () => {}
}
