import React from 'react'
import styled from "styled-components";
import {colors} from "../../constants/colors";

const Container = styled.button `
  border: none;
  background-color: white;
  -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  -moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  width: 32vw;
  height: 8.6vh;
  border-radius: 12px;
  position: fixed;
  bottom: 1vw;
  left: 0.6vw;
  z-index: 3;
`;
const Text = styled.p `
  font-family: 'Montserrat';
  color: ${colors["normal-secondary"]};
  font-size: 1.6em;
  font-weight: bold;
`;

export const OrdersCompletedComponent = props => (
  <Container>
    <Text>1800 <span style={{ color: colors.darkgray }}>Pedidos Completados</span></Text>
  </Container>
);
