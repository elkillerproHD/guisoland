import React from 'react';
import styled from "styled-components";
import {colors} from "../../../constants/colors";

const Container = styled.div `
  width: 100%;
  display: flex;
`;
const BtnStyle = styled.p `
  width: ${({percentage}) => percentage}%;
  border-bottom: 1px solid rgba(255,178,199,.5);
  font-family: 'Montserrat', sans-serif;
  text-align: center;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  margin: 0;
  padding-top: 1vh;
  padding-bottom: 1vh;
  color: ${colors["normal-secondary"]};
  cursor: pointer;
  &:hover {
    opacity: 0.4;
  }
`;
const BtnStyleSelected = styled.p `
  width: ${({percentage}) => percentage}%;
  border-bottom: 1px solid ${colors["normal-primary"]};
  font-family: 'Montserrat', sans-serif;
  text-align: center;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  margin: 0;
  padding-top: 1vh;
  padding-bottom: 1vh;
  color: ${colors["normal-primary"]};
  cursor: pointer;
  &:hover {
    opacity: 0.4;
  }
`;
const Button = (text, percentage, onChange, selected = false) => {


  if(selected){
    return <BtnStyleSelected percentage={percentage} onClick={onChange}>{text}</BtnStyleSelected>
  }
  return <BtnStyle percentage={percentage} onClick={onChange}>{text}</BtnStyle>
};
export const MultiSelectorsButton = props => {
  const { buttons } = props;
  const percentage = 100 / buttons.length;
  return(
    <Container>
      {
        buttons.map((btn, index) => {
          if(props.selected === index){
            return Button(btn, percentage, props.onChange, true)
          }
          return Button(btn, percentage, props.onChange)
        })
      }
    </Container>
  )
};
