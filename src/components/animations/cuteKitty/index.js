import React, {useEffect, useState} from "react";
import styled from "styled-components";
import kittyLookingCenter from "../../../icons/png/kittieLookingCenter.png";
import kittyLookingUp from "../../../icons/png/kittieLookingUp.png";
import kittyLookingEyesClosed from "../../../icons/png/kittieLookingEyesClosed.png";
import kittyLookingHalfEyesClosed from "../../../icons/png/kittieLookingHalfCloseEyes.png";

const KittyContainer = styled.img `
  width: 25vw;
  height: 24vh;
  position: absolute;
  left: 6vw;
  bottom: 0;
`

const imgsForAnimation = [
  kittyLookingCenter,
  kittyLookingUp,
  kittyLookingHalfEyesClosed,
  kittyLookingEyesClosed,
]

export const CuteKitty = () => {
  let [ imgState, setImgState ] = useState(0)

  const animationMoveUpEyes = () => {
    setTimeout(()=>setImgState(0), 10)
    setTimeout(()=>setImgState(1), 300)
    setTimeout(()=>setImgState(0), 1000)
    setTimeout(()=>setImgState(1), 3000)
    setTimeout(()=>setImgState(0), 3450)
    setTimeout(()=>setImgState(1), 3750)
    setTimeout(()=>setImgState(0), 4000)
  };
  const animationCloseEyes = () => {
    setTimeout(()=>setImgState(2), 10)
    setTimeout(()=>setImgState(3), 50)
    setTimeout(()=>setImgState(2), 100)
    setTimeout(()=>setImgState(0), 150)
  };

  useEffect(() => {
    animationMoveUpEyes()
    setTimeout(animationCloseEyes, 4120)
    setTimeout(animationCloseEyes, 4290)
    // animationCloseEyes()
    // setTimeout(animationCloseEyes,170)
    setInterval(() => {
      animationMoveUpEyes()
      setTimeout(animationCloseEyes, 4120)
      setTimeout(animationCloseEyes, 4290)
      // animationCloseEyes()
      // setTimeout(animationCloseEyes,170)
    }, 10000)
  }, []);
  return (
    <KittyContainer src={imgsForAnimation[imgState]} />
  )
};
