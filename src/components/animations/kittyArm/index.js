import React, {useEffect, useState} from 'react';
import styled, {keyframes} from "styled-components";
import KittyArmIcon from '../../../icons/png/kittyArm.png'
import {getRandomInt} from "../../../functions/random";
import {Flag} from "./flag";


const ArmImg = styled.img `
  height: 26vh;
  z-index: 0;
`

const appear = keyframes`
  0% { bottom: -56vh; }
  5%, 25% { bottom: -47vh; }
  30%{ bottom: -28vh;}
  34%{ bottom: -28vh;}
  35%{ bottom: -28vh;}
  38%{ bottom: -30vh;}
  41%{ bottom: -28vh;}
  44%{ bottom: -30vh;}
  47%{ bottom: -28vh;}
  50%{ bottom: -30vh;}
  52%{ bottom: -28vh;}
  54%{ bottom: -32vh;}
  56%{ bottom: -28vh;}
  58%{ bottom: -32vh;}
  60%{ bottom: -28vh;}
  62%{ bottom: -32vh;}
  64%{ bottom: -28vh;}
  66%{ bottom: -32vh;}
  64%{ bottom: -28vh;}
  65%{ bottom: -32vh;}
  66%{ bottom: -28vh;}
  67%{ bottom: -32vh;}
  68%{ bottom: -28vh;}
  69%{ bottom: -32vh;}
  70%{ bottom: -28vh;}
  71%{ bottom: -32vh;}
  72%{ bottom: -28vh;}
  73%{ bottom: -32vh;}
  74%{ bottom: -28vh;}
  75%{ bottom: -32vh;}
  76%{ bottom: -28vh;}
  77%{ bottom: -32vh;}
  79%{ bottom: -28vh;}
  81%{ bottom: -32vh;}
  83%{ bottom: -28vh;}
  85%, 95%{ bottom: -32vh;}
  100% { bottom: -56vh; }
`;

const shake = keyframes`
  0% { bottom: -56vh; }
  5%, 25% { bottom: -47vh; }
  30%{ bottom: -28vh; transform: rotate(0deg);}
  34%{ bottom: -28vh; transform: rotate(0deg);}
  35%{ bottom: -28vh; transform: rotate(7deg); }
  38%{transform: rotate(-7deg);}
  41%{ transform: rotate(7deg);}
  44%{transform: rotate(-7deg);}
  47%{transform: rotate(7deg);}
  50%{transform: rotate(-7deg);}
  53%{transform: rotate(0deg);}
  75%{bottom: -28vh; transform: rotate(0deg);}
  100% { bottom: -56vh; }
`;

const rotateAppear = keyframes `
  0%, 25% { left: 40vw; bottom: -31vh; transform: rotate(120deg);}
  30%, 80% { left: 40vw; bottom: -29vh; transform: rotate(35deg);}
  90%, 100% { left: 40vw; bottom: -31vh; transform: rotate(120deg);}

`

const animations = [
  appear, shake, rotateAppear
]

const Container = styled.div `
  position: absolute;
  bottom: -56vh;
  left: 32vw;
  height: 52vh;
  z-index: 2;
  animation: ${({anim}) => animations[anim]} 5s linear infinite;
  //animation: ${rotateAppear} 5s linear infinite;
`;

const SubContainer = styled.div `
  height: 52vh;
  position: relative;
`
export const KittyArmWithFlag = () => {
  let [ animationState, setAnimation ] = useState(2);

  useEffect(() => {
    const rand = getRandomInt(0, animations.length);
    setAnimation(rand)
    setInterval(()=>{
      const rand = getRandomInt(0, animations.length);
      setAnimation(rand)
    },5010)
  }, []);
  if(animationState === undefined){
    return null;
  }
  return(
    <Container anim={animationState}>
      <SubContainer>
        { animationState === 2 && <Flag /> }
        <ArmImg src={KittyArmIcon} />
      </SubContainer>
    </Container>
  )
};
