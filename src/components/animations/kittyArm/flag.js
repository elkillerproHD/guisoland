import React from 'react'
import styled from "styled-components";
import {getRandomInt} from "../../../functions/random";
import {colors} from "../../../constants/colors";

const Container = styled.div `
  width: 40vw;
  height: 25vh;
  background-color: white;
  box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.5);
  margin-bottom: -5vh;
  z-index: 2;
  position: absolute;
  top: -28vh;
  left: -8vw;
  transform: rotate(-35deg);
  color: ${colors["normal-secondary"]};
  justify-content: center;
  align-items: center;
  display: flex;
  font-size: 25px;
  padding: 0 5%;
`;

const words = [
  "¡Ver Pedidos Ahora!",
  "Si todos los restaurantes quebraron, pero nosotros seguimos aca, ¡Compra ahora!",
  "Por cada compra un gato virtual es rescatado de la calle de bytes",
  "Regalamos barbijos por cada compra, barbijos virtuales",
  "Le ponemos huevos a la cuarentena, aunque no tengamos por ser gatos virtuales",
  "Deja de salir con puro gato/a y compranos a nosotros",
  "Si estas leyendo esto solo quiero que sepas que tengo hambre",
  "Pin Pon se lava la carita con agua y con jabon. Vos lavate las manos sucio <3",
  "Nena donde esta mi super traje?!?!?! No se, pero compra en Guisoland cuando vuelvas"
]

export const Flag = (props) => {

  const rand = getRandomInt(0, words.length - 1);
  // const rand = 8
  return (
    <Container>
      {
        words[rand]
      }
    </Container>
  )
}
