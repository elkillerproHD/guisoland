import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import styled from "styled-components";
import {FoodCardComponent} from "../cards/foodCard";
import LeftArrowIcon from '../../icons/svg/slider-left-arrow.svg'
import RightArrowIcon from '../../icons/svg/slider-right-arrow.svg'
import {colors} from "../../constants/colors";

const ContainerLeft = styled.button `
  width: 4.5vw;
  height: 4.5vw;
  border: solid 1px #464646;
  background-color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: 0.2vw;
  border-radius: 6.5vw;
  top: 33%;
  outline: none;
  &:hover {
    background-color: ${colors["normal-primary"]};
  }
`;
const ContainerRight = styled.button `
  width: 4.5vw;
  height: 4.5vw;
  border: solid 1px #464646;
  background-color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0.2vw;
  border-radius: 6.5vw;
  top: 33%;
  outline: none;
  &:hover {
    background-color: ${colors["normal-primary"]};
  }
 `;
const Icon = styled.img `
    width: 3.5vw;
    height: 1.5vw;
`;

const CustomLeftArrow = ({ onClick }) => {
  return (
    <ContainerLeft onClick={() => onClick()} className="custom-left-arrow">
      <Icon src={LeftArrowIcon} />
    </ContainerLeft>
  )
};
const CustomRightArrow = ({ onClick }) => {
  return (
    <ContainerRight onClick={() => onClick()} src={LeftArrowIcon} className="custom-right-arrow">
      <Icon src={RightArrowIcon} />
    </ContainerRight>
  )
};

export const SliderComponent = props => (
  <Carousel
    customRightArrow={<CustomRightArrow />}
    customLeftArrow={<CustomLeftArrow />}
    additionalTransfrom={0}
    arrows
    centerMode={false}
    className=""
    containerClass="container-with-dots"
    draggable
    focusOnSelect={false}
    itemClass=""
    keyBoardControl
    minimumTouchDrag={80}
    renderButtonGroupOutside={false}
    renderDotsOutside={false}
    responsive={{
      desktop: {
        breakpoint: {
          max: 3000,
          min: 1024
        },
        items: 4,
        partialVisibilityGutter: 40
      },
      mobile: {
        breakpoint: {
          max: 464,
          min: 0
        },
        items: 2,
        partialVisibilityGutter: 30
      },
      tablet: {
        breakpoint: {
          max: 1024,
          min: 464
        },
        items: 4,
        partialVisibilityGutter: 30
      }
    }}
    sliderClass=""
    slidesToSlide={1}
    swipeable
  >
    <FoodCardComponent test/>
    <FoodCardComponent test/>
    <FoodCardComponent test/>
    <FoodCardComponent test/>
    <FoodCardComponent test/>
    <FoodCardComponent test/>
    <FoodCardComponent test/>
  </Carousel>
);

