import React from 'react'
import styled from "styled-components";
import {FoodMenu} from "../../models/foodMenu";
import {FoodCardComponent} from "../cards/foodCard";
import {devStorageServerUrl, storageServer} from "../../constants/urls";
import {colors} from "../../constants/colors";


const ContainerThumb = styled.div `
  width: 82vw;
  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #4f36d6 !important;
  }
  &::-webkit-scrollbar-thumb {
    background: #4f36d6 !important;
  }
`

const Container = styled.div `
  height: 100vh;
  display: flex;
  align-items: center;
  overflow: auto;
  overflow-y: hidden;
  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #4f36d6 !important;
  }
  &::-webkit-scrollbar-thumb {
    background: #4f36d6 !important;
  }
`
const SubContainer = styled.div `
  height: 95vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  justify-self: center;
  
`

export default class BuyPageMenuFoodSlider extends React.Component {
  state = {
    menus: [],
    renderMenus: []
  }

  componentDidMount() {
    this.Init()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { props } = this;
    if(prevProps.menus !== props.menus){
      console.log(props.menus)
      this.SetMenus();
    }
  }

  Init  = () => {
    this.SetMenus();
  }

  SetMenus = () => {
    const { menus } = this.props;
    const tempArray = [];
    const chunkSize = 2;
    for (let i = 0; i < menus.length; i += chunkSize) {
      const myChunk = menus.slice(i, i+chunkSize);
      // Do something if you want with the group
      tempArray.push(myChunk);
    }
    let url;
    const nodeEnv = process.env.NODE_ENV || "production";
    if(nodeEnv === "development"){
      url = `${devStorageServerUrl}/264/`;
    } else {
      url = `${storageServer}/264/`;
    }
    const toRender = tempArray.map((dta, index) => {
      const col = dta.map(dtaCol => <FoodCardComponent url={url} data={dtaCol} />)
      return <SubContainer>{col}</SubContainer>
    })
    this.setState({
      renderMenus: toRender
    })
    this.forceUpdate();
  }

  render() {
    const { renderMenus } = this.state;
    return (
      <ContainerThumb >
        <Container>
          { renderMenus }
        </Container>
      </ContainerThumb>
    );
  }
}
