import React from 'react';
import styled from "styled-components";
import ArrowDownIcon from '../../icons/js/arrowDown.svg'
import ArrowUpIcon from '../../icons/js/arrowUp.svg'
import {colors} from "../../constants/colors";
import {
  Link,
} from "react-router-dom";
const Container = styled.div`
  width: 18vw;
  overflow: hidden;
  height: 95vh;
  position: relative;

`

const SubContainer = styled.div `
  width: 18vw;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10px;
  

`
const Square = styled.div `
  cursor:pointer;
  width: 15vw;
  height: 15vw;
  background: #FFF;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.63);
  border-radius: 8px;
  margin-bottom: 20px;
  overflow: hidden;
`
const SquareSelected = styled.div `
  cursor: pointer;
  width: 15vw;
  height: 15vw;
  background: #FFF;
  box-shadow: 0px 3px 6px 0px rgba(255,178,199,1);
  border-radius: 8px;
  margin-bottom: 20px;
  overflow: hidden;
  border: 1px solid rgba(255,178,199,1);
`
const ImgSquare = styled.img `
  width: 20vw;
`
const ContainerImage = styled.div`
  width: 15vw;
  height: 10vw;
  padding-top: 1vw;
  display: block;
  overflow: hidden;
`
const TextSquare = styled.p `
  font-family: 'Montserrat',sans-serif;
  font-size: 18px;
  font-weight: bold;
  color: #ffb2c7;
  text-align: center;
  margin-top: 1vw;
`

export default class CategoryVerticalDraggableSlider extends React.Component {
  startY;
  scrollTop;
  isDown;

  static defaultProps = {
    categories: [],
  }

  state = {
    topArrow: true,
    bottomArrow: true,
  }

  onMouseDown = (e) => {
    const slider = document.getElementById("CategoryVerticalDraggableSlider")
    this.isDown = true;
    // slider.classList.add('active');
    this.startY = e.pageY - slider.scrollTop;
    this.scrollTop = slider.scrollTop;
  }
  onMouseLeave = (e) => {

    this.isDown = false;
  }
  onMouseUp = (e) => {
    this.isDown = false;
  }
  onMouseMove = (e) => {
    const slider = document.getElementById("CategoryVerticalDraggableSlider")
    if(!this.isDown) return;
    e.preventDefault();
    const y = e.pageY - slider.scrollTop;
    const walk = (y - this.startY) * 0.6; //scroll-fast
    this.ref.scrollTop = this.scrollTop - walk
  }

  componentDidMount() {
    const slider = document.getElementById("CategoryVerticalDraggableSlider")
    slider.addEventListener('mousedown',this.onMouseDown);
    slider.addEventListener('mouseleave',this.onMouseLeave);
    slider.addEventListener('mouseup',this.onMouseUp);
    slider.addEventListener('mousemove',this.onMouseMove);
  }

  render(){
    const { categories, url, selected, onClick } = this.props;
    const { topArrow, bottomArrow } = this.state;
    return [
      <Container
        ref={(ref) => this.ref=ref}
        id={"CategoryVerticalDraggableSlider"}
      >
        <SubContainer
          ref={(ref) => this.ref=ref}
          id={"CategoryVerticalDraggableSlider"}
        >
          {categories.map((cat, index) => {
            if(index === selected){
              return (
                <SquareSelected key={"square" + index}>
                  <ContainerImage>
                    <ImgSquare src={`${url}${cat.img.name}`} />
                  </ContainerImage>
                  <TextSquare>{cat.name}</TextSquare>
                </SquareSelected>
              )
            }
            return (
            <Square onClick={() => onClick(cat, index)} key={"square" + index}>
              <ContainerImage>
                <ImgSquare src={`${url}${cat.img.name}`} />
              </ContainerImage>
              <TextSquare>{cat.name}</TextSquare>
            </Square>
          )})}
        </SubContainer>
      </Container>,
      bottomArrow && <ArrowDownIcon fill={"white"} height={100} width={40} style={{ right: "-2vw", bottom: 0, position: "absolute" }} />,
      topArrow && <ArrowUpIcon fill={"white"} height={100} width={40} style={{ right: "-2vw", top: 0, position: "absolute" }} />
    ]
  }
}
