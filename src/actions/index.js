export const ScrollAction = () => {
  if(window.pageYOffset > 0){
    window.scrollTo(0, 0);
  } else {
    window.scroll({
      top: window.innerHeight * 0.15,
      left: 0,
      behavior: 'smooth'
    });
  }
};

export const InProccess = () => {
  alert("Estamos en proceso de contruccion para que pueda mejorar la usabilidad de esta web. Agredecemos tu interes, pronto estaran disponible todas las funciones.")
};
