import {
 TEST_INIT
} from "./actionTypes";

const initialState = {
    testState: false,
};

export default function reducer(state=initialState, action){

    switch (action.type){
        case TEST_INIT: return {...state,testState: true};
        default: return state;
    }
}
