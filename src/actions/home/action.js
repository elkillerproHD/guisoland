import {TEST_INIT} from "./actionTypes";
import Firebase from 'firebase';

export const TestAction = (view) => async (dispatch, getState) => {
	const db = Firebase.firestore();

	// ADD THESE LINES
	if (window.location.hostname === "localhost") {
		console.log("localhost detected!");
		db.settings({
			host: "localhost:8080",
			ssl: false
		});
	}

	dispatch({type: TEST_INIT, payload: view});
	return Promise.resolve()
};
