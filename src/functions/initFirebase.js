import Firebase from "firebase";

export default async function InitFirebase (){
  let firebaseConfig = {
    // apiKey: "AIzaSyD7gdyNA7XY8EVZ2VQ65Zm5KV8QS7pat8w",
    // authDomain: "guisoland-b369a.firebaseapp.com",
    // databaseURL: "https://guisoland-b369a.firebaseio.com",
    projectId: "guisoland-b369a",
    // storageBucket: "guisoland-b369a.appspot.com",
    // messagingSenderId: "71568274698",
    // appId: "1:71568274698:web:b43a6b70eb8aebc5f976f3",
    // measurementId: "G-R63NW4XLXH"
  };
  try {
    await Firebase.initializeApp(firebaseConfig);
    return Promise.resolve();
  } catch (e) {
    return Promise.reject()
  }

}
