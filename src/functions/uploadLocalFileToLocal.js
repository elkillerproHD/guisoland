import Path from "path";
import Fs from "fs";
import Jimp from 'jimp';
const storagePath = Path.join(process.cwd(), '/storage');

export default async function UploadLocalFileToLocal(data={}){
  const { path= {}, name='', fileType='', handleError=()=>{}, sizes=[], isImage=false } = data;
  const gcsname = Date.now() + name;

  if(!isImage){
    try {
      const file = Fs.readFileSync(path);
      console.log(file)
      console.log(`${Path.join(storagePath)}/${gcsname}`)
      if (!Fs.existsSync(storagePath)){
        Fs.mkdirSync(storagePath);
      }
      Fs.writeFile(`${Path.join(storagePath)}/${gcsname}`, file, function(err, result) {
        if(err) console.log('error', err);
      });
      return Promise.resolve(gcsname)
    } catch (err) {
      handleError(err)
      console.log(err)
    }
  } else {
    console.log(sizes)
    for(let i = 0; i < sizes.length; i++){
      const size = sizes[i];
      console.log(size)
      const image = await Jimp.read(path);
      console.log("image")
      console.log(image)
      const sizegcsname = `${size}/${gcsname}`;
      await image.resize(size, Jimp.AUTO);
      await image.writeAsync(`${storagePath}/${sizegcsname}`);
    }
    try {
      const file = Fs.readFileSync(path);
      console.log(file)
      console.log(`${Path.join(storagePath)}/${gcsname}`)
      // if (!Fs.existsSync(storagePath)){
      //   Fs.mkdirSync(storagePath);
      // }
      // Fs.writeFile(`${Path.join(storagePath)}/${gcsname}`, file, function(err, result) {
      //   if(err) console.log('error', err);
      // });
      return Promise.resolve(gcsname)
    } catch (err) {
      handleError(err)
      console.log(err)
    }
  }
}
