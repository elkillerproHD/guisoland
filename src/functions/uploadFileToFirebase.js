import Firebase from 'firebase'
import '@firebase/storage';
import {storageUrl} from "../storageServer/constants";

export async function UploadFileToFirebaseFront(data={}){
  const { file= {}, handleError=()=>{}, sizes= [] } = data;
  const nodeEnv = process.env.NODE_ENV || "production";
  const fileName = Date.now() + file.name;
  console.log(file)

  if(nodeEnv === "development"){
    const formData = new FormData();
    formData.append("file", file)
    formData.append("name", fileName)
    await fetch(storageUrl, {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));

  } else if(nodeEnv === "production"){
    const storageRef = Firebase.storage().ref(fileName);
    storageRef.put(file)
  }
  return Promise.resolve(fileName)
}
