import firebase from 'firebase'
Promise.all(firebase.apps().map(app => app.delete()));

/*

curl -v -X DELETE "http://localhost:8080/emulator/v1/projects/guisoland-b369a/databases/(default)/documents"

* */
