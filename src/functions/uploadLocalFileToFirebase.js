import Fs from "fs";
import Path from 'path'
import {Storage} from "@google-cloud/storage";
import EnvVariables from '../env.variables.json'

// const imagePath = Path.join(__dirname, '../migrations/seeds/foodCategory/images/arroz.jpg');
const storagePath = Path.join(process.cwd(), '/storage');

export default async function UploadLocalFileToFirebase(data={}){
  const { path= {}, name='', fileType='', handleError=()=>{}, isPublic=false } = data;
  try {
    const file = Fs.readFileSync(path);
    const nodeEnv = process.env.NODE_ENV || "production";
    const FirebaseConstants = require(`../../${EnvVariables[nodeEnv]["firebaseConfigFile"]}`)
    const storageRef = new Storage(
      {
        projectId: FirebaseConstants.project_id,
        keyFilename: EnvVariables[nodeEnv]["firebaseConfigFile"],
      }
    )
    console.log(file)
    const bucket = storageRef.bucket(`${FirebaseConstants.project_id}.appspot.com`)
    const gcsname = Date.now() + name;
    const fileBucket = bucket.file(gcsname);
    const stream = fileBucket.createWriteStream({
      metadata: {
        contentType: fileType
      },
      resumable: false
    });

    stream.on('error', (err) => {
      console.log("err")
      console.log(err)
    });

    stream.on('finish', () => {
      console.log("Finish")
      console.log(gcsname)
      console.log(fileBucket)
    });

    stream.end(file);
    if(isPublic){
      await fileBucket.makePublic();
    }
    return Promise.resolve(gcsname)
  } catch (err) {
    handleError(err)
    console.log(err)
  }
}

