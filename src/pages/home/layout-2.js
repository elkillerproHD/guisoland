import React from 'react'
import {BackgroundImage} from "../../components/containers/backgroundImage";
import BackgroundIMG from '../../images/highQuality/highQualityBurger1.jpg'
import PinkTriangleIcon from '../../icons/svg/triangleLayout2.svg'
import styled from "styled-components";
import {colors} from "../../constants/colors";
import {strings} from "../../constants/texts";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;
const FloatPinkTriangle = styled.img `
  width: 100vw;
  height: 100vw;
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 1;
`;
const TextTitle = styled.h2 `
  font-family: 'Caveat', cursive;
  font-size: 11em;
  text-align: center;
  color: ${colors["normal-secondary"]};
  z-index: 2;
  margin: 0;
`;
const TextSubTitle = styled.h3 `
  font-family: 'Montserrat', sans-serif;
  font-size: 2em;
  text-align: left;
  color: white;
  z-index: 2;
  width: 35vw;
  margin: 0;
`;

export const HomeLayout2 = props => {
  return(
    <BackgroundImage color={'rgba(0,0,0,0.4)'} modal src={BackgroundIMG}>
      <Container>
        <TextTitle>{strings.Home_Layout2_BigText}</TextTitle>
        <TextSubTitle>{strings.Home_Layout2_SubTitle1}<br/>{strings.Home_Layout2_SubTitle2}</TextSubTitle>
        <FloatPinkTriangle src={PinkTriangleIcon} />
      </Container>
    </BackgroundImage>
  )
};
