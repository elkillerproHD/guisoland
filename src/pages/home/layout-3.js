import React from 'react';
import {BackgroundImage} from "../../components/containers/backgroundImage";
import BackgroundIMG from "../../images/highQuality/highQualityBurger2.jpg"
import {Background} from "../../components/containers/background";
import styled from "styled-components";
import {MenusFiltersComponent} from "../../components/menusFilters";
import {SliderComponent} from "../../components/slider";
import {strings} from "../../constants/texts";
// import {OrdersCompletedComponent} from "../../components/buttons";
// import {OpenCloseButtonIcon} from "../../icons/js/openCloseButton";

const Container = styled.div `
  width: 99%;
  height: 98%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  padding-bottom: 2%;
`;

const Text = styled.p `
  font-family: 'Montserrat';
  font-weight: bold;
  font-size: 2em;
  color: white;
  margin-left: 3vw;
  margin-top: 3vh;
  margin-bottom: 3vh;
`;

const menusFounds = 5;

export const HomeLayout3 = props => {
  return (
    <BackgroundImage width={"99vw"} image={BackgroundIMG} >
      <Container>
        <MenusFiltersComponent />
        <div style={{ width: "100%", height: 2 + "vh" }} />
        <Background width={"97vw"} height={"75vh"} color={'rgba(255, 178, 199, 0.79)'}>
          <Text>{strings.MenusFound + menusFounds}</Text>
          <SliderComponent />
        </Background>
        {/*<OrdersCompletedComponent />*/}
        {/*<OpenCloseButtonIcon />*/}
      </Container>
    </BackgroundImage>
  )
};
