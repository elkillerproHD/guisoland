import React from "react";
import styled from "styled-components";
import {colors} from "../../constants/colors";
import {CardWithFunnyCorners} from "../../components/layouts/cardWithFunnyCorners";
import {Background} from "../../components/containers/background";
import {CardHomeLanding} from "../../components/cards/cardHomeLanding";
import {strings} from "../../constants/texts";
// import {BottomArrowIcon} from "../../icons/js/arrow";
// import {ScrollAction} from "../../actions";
// import {connect} from "react-redux";
// import {HomeReducerName} from "../../actions/home";
// import {TestAction} from "../../actions/home/action";
// import {ScrollAction} from "../../actions";

const Container = styled.div `
  width: 100%;
  height: 89%;
  position: relative;
`;
const TopCallToActionText1 = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 1.2em;;
  font-weight: bold;
  color: ${colors["normal-secondary"]};
  display: inline-block;
  margin-left: 2vw;
  margin-top: 2vh;
`;
const TopCallToActionText2 = styled.p `
  font-family: 'Montserrat', sans-serif;
  font-size: 1.2em;
  font-weight: bold;
  color: ${colors.darkgray};
  display: inline-block;
  margin-left: 0.5vw;
  margin-top: 2vh;
`;
const CardMenusContainer = styled.div `
  width: 90%;
  padding-left: 5%;
  padding-right: 5%;
  display: flex;
  margin-top: 5vh;
  justify-content: space-between;
`;
const MarginMiddleCard = styled.div `
  margin-top: 2vh;
  margin-bottom: -2vh;
  animation-delay: 5s;
  -webkit-animation:shake-bottom 5s cubic-bezier(.455,.03,.515,.955) infinite both;
  animation:shake-bottom 5s cubic-bezier(.455,.03,.515,.955) infinite both
`;


const HomeLayout1 = () => {

  return(
    <Background color={colors["normal-primary"]}>
      <CardWithFunnyCorners >
        <Container id={"Container"}>
          <TopCallToActionText1>{strings.Home_Layout1_TopCallToActionText1}</TopCallToActionText1>
          <TopCallToActionText2>{strings.Home_Layout1_TopCallToActionText2}</TopCallToActionText2>
          <CardMenusContainer>
            <CardHomeLanding cardColor={colors["normal-secondary"]} />
            <MarginMiddleCard>
              <CardHomeLanding cardColor={colors["normal-primary"]} />
            </MarginMiddleCard>
            <CardHomeLanding cardColor={colors.darkgray} />
          </CardMenusContainer>

        </Container>
      </CardWithFunnyCorners>
    </Background>
  )
};

export default HomeLayout1;
