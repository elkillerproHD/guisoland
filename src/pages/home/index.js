import React from "react";
import HomeLayout1 from "./layout-1";
import {HomeLayout2} from "./layout-2";
import {HomeLayout3} from "./layout-3";
import {MapForLanding} from "../../components/maps";
import {FooterComponent} from "../../components/footer";
import HeaderComponent from "../../components/header";
import {KittyLayout} from "./kittyLayout";
import {FoodCategory} from "../../models/foodCategory";
import {FoodMenu} from "../../models/foodMenu";
// import {GetSeed as foodCategorySeed} from "../../migrations/seeds/foodCategory";
// import {MultipleWriteJson} from "../../models/baseModel";

class HomePage extends React.Component {
  componentDidMount() {
    this.Check()
  }
  Check = async() => {
    const foodCategory = new FoodCategory({
      img: {},
      name: "Caca"
    })
    const foodMenu = new FoodMenu({})
    // await foodCategory.findById({id:"milanesas-category"});
    // await foodCategory.create({success: (result)=>{console.log(result)}});
    // const json = [
    //   ...foodCategorySeed(),
    // ]
    // await MultipleWriteJson(json)
    await foodCategory.all();
    await foodMenu.all();
    // console.log(foodCategory)
    console.log(foodCategory.values)
    console.log(foodMenu.values)
  }
  render(){
    return [
      <HeaderComponent key={"HeaderComponentHomePage"}/>,
      <KittyLayout key={"KittyLayoutHomePage"} />,
      <HomeLayout1 key={"HomeLayout1HomePage"} />,
      <HomeLayout2 key={"HomeLayout2HomePage"} />,
      <HomeLayout3 key={"HomeLayout3HomePage"} />,
      <MapForLanding key={"MapForLandingHomePage"} />,
      <FooterComponent key={"FooterComponentHomePage"} />
    ]
  }
}

export default HomePage;
