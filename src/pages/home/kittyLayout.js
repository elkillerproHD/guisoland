import React from 'react';
import styled from "styled-components";
import {CuteKitty} from "../../components/animations/cuteKitty";
import {Background} from "../../components/containers/background";
import {BigCardHomeLanding} from "../../components/cards/cardHomeLanding/big";
import {KittyArmWithFlag} from "../../components/animations/kittyArm";
import {colors} from "../../constants/colors";
import {ScrollAction} from "../../actions";
import {BottomArrowIcon} from "../../icons/js/arrow";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
`
const ContainerCard = styled.div `
  position: absolute;
  top: 3vh;
  left: 7.5vw;
  animation-delay: 5s;
  -webkit-animation:shake-bottom 5s cubic-bezier(.455,.03,.515,.955) infinite both;
  animation:shake-bottom 5s cubic-bezier(.455,.03,.515,.955) infinite both;
`
const LeftCol = styled.div `
  width: 40vw;
  height: 100vh;
  position: relative;
`;
const RightCol = styled.div `
  width: 60vw; 
  padding-left: 5vw;
  padding-top: 2vh;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const Title = styled.p `
  font-size: 55px;
  font-weight: bold;
  color: ${colors["normal-secondary"]};
  text-align: left;
  width: 48vw;
  font-family: 'Montserrat';
`
const SubTitle = styled.p `
  font-size: 31px;
  width: 48vw;
  margin-top: 5vh;
  font-weight: bold;
  color: ${colors.darkgray};
  text-align: left;
  font-family: 'Montserrat';
`
const BottomArrowButton = styled.div `
  position: absolute;
  left: 40vw;
  right: 0;
  margin: auto;
  width: 3.5vw;
  bottom: 3%;
  cursor: pointer;
  -webkit-animation: pulsate-fwd 0.5s ease-in-out infinite both;
  animation: pulsate-fwd 0.5s ease-in-out infinite both;
  z-index: 1;
  &:hover {
  -webkit-animation: pulsate-fwd 0s ease-in-out both;
  animation: pulsate-fwd 0s ease-in-out both;
  }
`;
const BottomArrowButtonLine = styled.div `
  width: 100%;
  height: 0.8vh;
  background-color: ${colors["normal-secondary"]};
  margin-top: -1vh;
`;
const Texts = {
  title: "La forma definitiva para comprar viandas de comida diarias",
  content: [
    "Descubre una nueva forma de comprar comida,",
    "ahorra tiempo y dinero con Guisoland",
    ". Viandas empresariales a demanda, al hacer los pedido con anticipación  y en cantidad nos permite brindarle un servicio mas económico que la competencia.",
  ]
}

export const KittyLayout = () => {
  return(
    <Background color={"#fff"} >
      <Container>
        <LeftCol>
          <ContainerCard>
            <BigCardHomeLanding />
          </ContainerCard>
          <CuteKitty />
          <KittyArmWithFlag />
        </LeftCol>
        <RightCol >
          <Title>{Texts.title}</Title>
          <SubTitle>
            {Texts.content[0]}
            <span style={{color: colors["normal-secondary"]}}>{Texts.content[1]}</span>
            {Texts.content[2]}
          </SubTitle>
          <BottomArrowButton onClick={ScrollAction}>
            <BottomArrowIcon
              size={window.innerWidth * 0.035}
              color={colors["normal-secondary"]}
            />
            <BottomArrowButtonLine/>
          </BottomArrowButton>
        </RightCol>
      </Container>
    </Background>
  )
};
