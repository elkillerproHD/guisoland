import React from 'react'
import styled from "styled-components";
import CategoryVerticalDraggableSlider from "../../components/slider/categoryVerticalDraggableSlider";
import {devStorageServerUrl, storageServer} from "../../constants/urls";
import {FoodCategory} from "../../models/foodCategory";
import BuyPageMenuFoodSlider from "../../components/slider/buyPageSlider";
import {FoodMenu} from "../../models/foodMenu";
import {colors} from "../../constants/colors";
import {Link} from "react-router-dom";

const Container = styled.div `
  width: 99vw;
  padding-left: 1vw;
  display: flex;
  background: ${colors["normal-secondary"]};
`
const CategoriesAndBack = styled.div `
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  position: relative;
`
const BackText = styled.div `
  width: 50px;
  text-decoration: none;
  font-weight: bold;
  font-family: 'Montserrat';
  color: ${colors.white};
  font-size: 1em;
  text-align: center;
  padding-left: 5%;
  padding-right: 5%;
  margin-top: 0.5vh;
  word-break: break-all;
  white-space: normal;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`

export default class BuyPage extends React.Component {
  state = {
    categories: [],
    menus: [],
    selected: 0,
  }
  componentDidMount() {
    const nodeEnv = process.env.NODE_ENV || "production";
    if(nodeEnv === "development"){
      this.url = `${devStorageServerUrl}/264/`;
    } else {
      this.url = `${storageServer}/264/`;
    }
    this.InitCategories().then()
  }
  ClickCategories = (cat, index) => {
   this.setState({
     selected: index
   })
    setTimeout(this.SetMenusByCategory, 200)
  }
  SetMenusByCategory = async () => {
    const { categories, selected } = this.state;
    const foodMenu = new FoodMenu({})
    await foodMenu.findByValues({
      type: "categoriesIds",
      condition: "array-contains",
      value: categories[selected].id
    })
    if(foodMenu.values){
      this.setState({
        menus: foodMenu.values
      })
    } else {
      this.setState({
        menus: []
      })
    }

  }
  InitCategories = async () => {
    const foodCategory = new FoodCategory({})
    await foodCategory.all();
    this.setState({
      categories: foodCategory.values
    })
    this.SetMenusByCategory()
  }
  render(){
    const { categories, selected, menus } = this.state;
    const { ClickCategories, url } = this;
    const backUrl = window.location.href
    return(
      <Container >
        <CategoriesAndBack>
          <Link style={{ textDecoration:"none" }} to={"/"} >
            <BackText >Atras</BackText>
          </Link>
          <CategoryVerticalDraggableSlider
            selected={selected}
            onClick={ClickCategories}
            url={url}
            categories={categories}
          />
        </CategoriesAndBack>
        <BuyPageMenuFoodSlider menus={menus} />
      </Container>
    )
  }
}
