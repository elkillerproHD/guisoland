import FireModel from "./baseModel";

export const collection = "FOOD-MENU"

/*

{
  name,
  img,
  descrip,
  price,
  lowerPrice,
  smallWord,
  discout,
  tagsId,
  categoryId
}

*/

export const model = [
  {
    name: "categoriesIds",
    type: "array",
    required: true,
  },
  {
    name: "img",
    type: "img|string|object",
    required: true,
  },
  {
    name: "name",
    type: "string",
    required: true,
  },
  {
    name: "descrip",
    type: "string",
    required: true,
  },
  {
    name: "price",
    type: "number",
    required: true,
  },
  {
    name: "lowerPrice",
    type: "number",
    required: false,
  },
  {
    name: "smallWord",
    type: "string",
    required: true,
  },
  {
    name: "discount",
    type: "number",
    required: false,
  },
  {
    name: "tagsId",
    type: "array",
    required: false,
  },
]

export class FoodMenu extends FireModel {
  constructor(data) {
    super({
      collection: collection,
      data,
      model
    });
  }
}
