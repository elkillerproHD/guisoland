import FireModel from "./baseModel";

export const collection = "FOOD-CATEGORY"

export const model = [
  {
    name: "img",
    type: "img|string|object",
    required: true,
  },
  {
    name: "name",
    type: "string",
    required: true,
  },

]

export class FoodCategory extends FireModel {
  constructor(data) {
    super({
      collection: collection,
      data,
      model
    });
  }
}

