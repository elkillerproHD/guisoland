import Firebase from "firebase";
import {validateModel} from "../functions/validateModel";
import {typedef} from "bycontract";
const uploadFileFirebasePath = "../functions/uploadLocalFileToFirebase"
const uploadFileLocalPath = "../functions/uploadLocalFileToLocal"
// import {UploadLocalFileToFirebase, UploadLocalFileToLocal} from "../functions/uploadLocalFileToFirebase";
// import {collection} from "./foodCategory";

export const imgType = {
  name: "string",
  path: "string",
  fileType: "string",
  file: "boolean"
}

typedef("img", imgType)

export function setNewImage({data}){
  const {name="", local=false} = data;
  return {name, local}
}

/*
*  JSON de ejemplo para realizar multiple writes
* [{
* // CHECK es el modelo a utilizar en la vadilacion con bycontract
*   check: foodModel
*   model: "FOOD-CATEGORY",
*   action: "add",
* // DATA tiene que ser el modelo normal con los datos
*   data: [
*     name: "Pastas",
*     img: {
*       name: "pastas.png",
*       path: "asd/pastas.png",
*       fileType: "image/png"
*     }
*   ]
* }]
* */

const errorCondition = "The Condition is not valid. The valid contions are == != < <= > >= and your condition is ";
const errorInSearchParameters = "You need to pass valid parameters ({type, value})"
const errorID = "You must provide an ID for search"
const errorIDMany2Many = "You must provide an ID in the constructor to7 work with many2many"
const errorMany2ManyFields = "You must provide collection, name and table name in the model of Many2Many"

const preModels = {
  name: "id",
  type: "string",
  required: true
}
const preModelsMany2Many = [
  {
    name: "name",
    type: "string",
    required: true
  },
  {
    name: "collection",
    type: "string",
    required: true
  },
  {
    name: "tableName",
    type: "string",
    required: true
  },

]
export async function CheckAndUploadFiles(data){
  // This Function get by params the validated data for upload to firestore.
  // The next last step before upload it is, check if some property is a file
  // For every file in the data, we going to upload it to Firebase if we are in production
  // or in the /<rootOfYourFolder>/storage if we are in development
  // and property of fyle by property of file also change de value of the property for
  // the url of the uploaded file
  const keys = Object.keys(data);
  const values = Object.values(data);
  for(let i = 0; i < values.length; i++){
    const value = values[i];
    if(value.file){
      const nodeEnv = process.env.NODE_ENV || "production";
      if(nodeEnv === "development"){
        const UploadLocalFileToLocal = await import(uploadFileLocalPath)
        const name = await UploadLocalFileToLocal.default(value)
        data[keys[i]] = {
          local: true,
          name
        }
      } else if(nodeEnv === "production"){
        const UploadLocalFileToFirebase = await import(uploadFileFirebasePath)
        const name = await UploadLocalFileToFirebase.default({...value, isPublic: true})
        data[keys[i]] = {
          local: false,
          name
        }
      }
    }
  }
  delete data.file;
  return Promise.resolve(data);
}

export default class FireModel {
  constructor(props) {
    this.db = Firebase.firestore();
    this.data = props.data;
    this.collection = props.collection;
    if (window.location.hostname === "localhost") {
      console.log("localhost detected!");
      this.db.settings({
        host: "localhost:8080",
        ssl: false
      });
    }
    this.model = props.model;
    this.modelRef = this.db.collection(this.collection)
  }

  create = async (dta= {}) => {
    const {success=function(){},fail=function(){}} = dta;
    const { modelRef, model } = this;
    let { data } = this;
    validateModel(model, data)
    data = await CheckAndUploadFiles(data)
    await modelRef.add(data)
      .then((doc) => {
        success(doc)
        return Promise.resolve(doc)
      })
      .catch(function(error) {
        fail(error)
        return Promise.reject(error)
      });
  }
  all = async (dta= {}) => {
    const {success=function(){},fail=function(){}} = dta;
    const { modelRef } = this;
    let query = modelRef.get();
    await query.then((doc) => {
      if(!doc.empty){
        const values = doc.docs.map(dc => ({id: dc.id, ...dc.data()}));
        this.values = values;
        success(this.values);
        return Promise.resolve(values);
      } else {
        fail()
        return Promise.resolve()
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
      fail(error)
      return Promise.reject(error)
    });
  };
  set = async (dta= {}) => {
    const {success=function(){},fail=function(){}} = dta;
    const { modelRef, model, data } = this;
    validateModel(model.push(preModels), data)
    let noIdData = data;
    delete noIdData.id;
    noIdData = await CheckAndUploadFiles(noIdData)
    await modelRef.doc(data.id).set(noIdData)
      .then((doc) => {
        success(doc)
        return Promise.resolve(doc)
      })
      .catch(function(error) {
        fail(error)
        return Promise.reject(error)
      });
  }
  update = async (dta= {}) => {
    const {success=function(){},fail=function(){}} = dta;
    const { modelRef, data, model } = this;
    validateModel(model.push(preModels), data)
    let noIdData = data;
    delete noIdData.id;
    noIdData = await CheckAndUploadFiles(noIdData)
    await modelRef.doc(data.id).update(noIdData)
      .then((doc) => {
        success(doc)
        return Promise.resolve(doc)
      })
      .catch(function(error) {
        fail(error)
        return Promise.reject(error)
      });
  }
  delete = async (dta= {}) => {
    const {success=function(){},fail=function(){}} = dta;
    const { modelRef, db, data } = this;
    validateModel(preModels, data)
    const deleteRef = db.collection(`${this.collection}_deleted`)
    const noIdData = data;
    delete noIdData.id;
    await deleteRef.doc(data.id).set(noIdData)
      .catch(function(error) {
        fail(error)
        return Promise.reject(error)
      });
    await modelRef.doc(data.id).delete()
      .catch(function(error) {
        fail(error)
        return Promise.reject(error)
      });
    return Promise.resolve()
  }
  findById = async (dta) => {
    const {id=false,success=function(){}, fail=function(){}} = dta;
    const { modelRef } = this;
    if(!id){
      fail(errorID)
      console.error(errorID)
      return
    }
    console.log(id)
    await modelRef.doc(id).get().then((doc) => {
      console.log(doc)
      const queryData = {id: doc.id, ...doc.data()}
      success(queryData)
      this.values = queryData;
      return Promise.resolve(doc)
      if(doc.exists){
        const queryData = {id: doc.id, ...doc.data()}
        success(queryData)
        this.values = queryData;
        return Promise.resolve(doc)
      } else {
        fail()
        return Promise.resolve()
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
      fail(error)
      return Promise.reject(error)
    });
  }
  findByValues = async (dta) => {
    const {
      type=false,
      value=false,
      condition=undefined,
      orderBy=false,
      limit=false,
      success=function(){},
      fail=function(){}
    } = dta;
    const { modelRef } = this;
    if(!type || !value){
      fail(errorInSearchParameters)
      console.error(errorInSearchParameters)
      return
    }
    if(
      condition !== false
      && condition !== "=="
      && condition !== "<"
      && condition !== ">"
      && condition !== ">="
      && condition !== "<="
      && condition !== "array-contains"
    ){
      fail(`${errorCondition}${condition}`)
      console.error(errorCondition, condition)
      return
    }
    let query = modelRef
    if(condition){
      query = query.where(type, condition, value)
    }
    if(orderBy){
      query = query.orderBy(orderBy)
    }
    if(limit){
      query = query.limit(limit)
    }
    await query.get().then((doc) => {
      if(!doc.empty){
        this.values = doc.docs.map(doc => ({id: doc.id, ...doc.data()}))
        success(doc)
        return Promise.resolve(this.values)
      } else {
        fail()
        return Promise.resolve()
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
      fail(error)
      return Promise.reject(error)
    });
    // this.keys=query.keys();
    // query.keys().forEach((opt) => {
    //   this[opt] = query[opt]
    // })
    // console.log(this)
  }
  multipleWrites = async (dta) => {
    const {success=function(){}, fail=function(){}, data=[]} = dta;
    const { modelRef, db, model } = this;
    const toMap = [];
    let objects = [];
    dta.map((query) => {
      if(objects.length < 11){
        objects.push(query)
      } else {
        toMap.push(objects);
        objects = [];
      }
    })
    if(objects.length > 0){
      toMap.push(objects);
    }
    for (const dta of toMap) {
      const batch = db.batch();
      for(let query in dta){
        let noIdData = query;
        switch (query.type) {
          case "add":
            validateModel(model, query.data)
            const docRef = modelRef.doc();
            query.data = await CheckAndUploadFiles(query.data)
            batch.set(docRef, query.data);
            break;
          case "set":
            validateModel(model.push(preModels), data)
            delete noIdData.id;
            noIdData= await CheckAndUploadFiles(noIdData)
            batch.set(modelRef.doc(query.data.id), noIdData)
            break;
          case "delete":
            const deleteRef = db.collection(`${this.collection}_deleted`)
            delete noIdData.id;
            batch.set(deleteRef.doc(query.data.id), noIdData)
            batch.delete(modelRef.doc(query.data.id))
            break;
          case "update":
            validateModel(model.push(preModels), data)
            delete noIdData.id;
            noIdData= await CheckAndUploadFiles(noIdData)
            batch.update(modelRef.doc(query.data.id), noIdData)
            break;
          default:
        }
      }
      batch.commit();
    }
  }
}

export function MultipleWriteJson(props) {
  const { data, isFront } = props;
  const db = Firebase.firestore();
  const dev = true;
  if (dev) {
    console.log("localhost detected!");
    db.settings({
      host: "localhost:8080",
      ssl: false
    });
  }
  // Primero agrupamos las mismas colecciones
  const sameCollections = {};
  data.forEach((dta) => {
    if(sameCollections[dta.model]){
      sameCollections[dta.model].push(dta)
    } else {
      sameCollections[dta.model] = [];
      sameCollections[dta.model].push(dta)
    }
  })
  Object.values(sameCollections).forEach((dta, index) => {
    const toMap = [];
    let objects = [];
    const actualKey = Object.keys(sameCollections)[index]
    dta.map((query) => {
      if(objects.length < 11){
        objects.push(query)
      } else {
        toMap.push(objects);
        objects = [];
      }
    })
    if(objects.length > 0){
      toMap.push(objects);
    }
    toMap.forEach(async (dta) => {
      const batch = db.batch();
      for (const query of dta){
        let noIdData = query;
        switch (query.action) {
          case "add":
            validateModel(query.check, query.data)
            const docRef = db.collection(actualKey).doc();
            query.data = await CheckAndUploadFiles(query.data)
            batch.set(docRef, query.data);
            break;
          case "set":
            query.check.push(preModels)
            validateModel(query.check, query.data)
            delete noIdData.id;
            noIdData = await CheckAndUploadFiles(query.data)
            batch.set(db.collection(actualKey).doc(query.data.id), noIdData)
            break;
          case "delete":
            const deleteRef = db.collection(`${actualKey}_deleted`)
            delete noIdData.id;
            batch.set(deleteRef.doc(query.data.id), noIdData)
            batch.delete(db.collection(actualKey).doc(query.data.id))
            break;
          case "update":
            query.check.push(preModels)
            validateModel(query.check, query.data)
            delete noIdData.id;
            noIdData = await CheckAndUploadFiles(noIdData)
            batch.update(db.collection(actualKey).doc(query.data.id), noIdData)
            break;
          default:
        }
      }
      batch.commit().then(function() {
        console.log('Done.')
      })
        .catch(err => console.log(`There was an error: ${err}`))
    })
  })
}
